﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityStore;
using System.Data.SqlClient;
using System.Data;

namespace DTLStore
{
    /*! 
    *  \brief     This is a data access class.  
    *  \details   This class allows us to access the users in the database
    *  \author    Yakupcan ERGEN
    *  \date      03.06.2021
    */
    public class DALUserLog
    {
        public static List<EntityUserLog> userLogList(int userId)
        {
            Baglanti baglanti = Baglanti.GetInstance();
            List<EntityUserLog> userLogs = new List<EntityUserLog>();
            SqlCommand komut4 = new SqlCommand("Select * from LogTable where log_userId=" + userId, baglanti.bgl);
            if (komut4.Connection.State != ConnectionState.Open)
                komut4.Connection.Open();
            SqlDataReader dr = komut4.ExecuteReader();
            while (dr.Read())
            {
                EntityUserLog ent = new EntityUserLog();
                ent.CustomerId = int.Parse(dr["log_UserId"].ToString());
                ent.CustomerName= dr["log_userName"].ToString();
                ent.CustomerEvent = dr["log_UserEvent"].ToString();
                ent.CustomerEventTimeStampt = Convert.ToDateTime(dr["log_userEventTimeStampt"].ToString());
                userLogs.Add(ent);
            }
            dr.Close();
            return userLogs;
        }
        public static int UserLogAdd(EntityUserLog userLog)
        {
            SqlCommand komut = new SqlCommand("insert into LogTable (log_userId,log_userName,log_userEvent,log_userEventTimeStampt) VALUES (@P1,@P2,@P3,@P4)", Baglanti.GetInstance().bgl);
            if (komut.Connection.State != ConnectionState.Open)
                komut.Connection.Open();
            komut.Parameters.AddWithValue("@P1", userLog.CustomerId);
            komut.Parameters.AddWithValue("@P2", userLog.CustomerName);
            komut.Parameters.AddWithValue("@P3", userLog.CustomerEvent);
            komut.Parameters.AddWithValue("@P4", userLog.CustomerEventTimeStampt);
            return komut.ExecuteNonQuery();
        }
    }
}
