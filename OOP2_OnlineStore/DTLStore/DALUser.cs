﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityStore;
using System.Data.SqlClient;
using System.Data;
namespace DTLStore
{
    /*! 
    *  \brief     This is a data access class.  
    *  \details   This class allows us to access the users in the database
    *  \author    Sabri Sinan Sağlam
    *  \date      30.05.2021
    */
    public class DALUser
    {
        /// <summary> 
        /// Add all users in the database to the EntityUser list
        /// </summary>
        /// <returns>
        /// returns a list of all users in the database
        /// </returns>
        public static List<EntityUser> UserList()
        {
            Baglanti baglanti = Baglanti.GetInstance();
            List<EntityUser> UserList = new List<EntityUser>();
            SqlCommand komut3 = new SqlCommand("Select * from UserDataBase", baglanti.bgl);
            if (komut3.Connection.State != ConnectionState.Open)
                komut3.Connection.Open();
            SqlDataReader dr = komut3.ExecuteReader();
            while (dr.Read())
            {
                EntityUser ent = new EntityUser();
                ent.UserId = int.Parse(dr["userId"].ToString());
                ent.UserName = dr["userName"].ToString();
                ent.UserFirstName = dr["userFirstName"].ToString();
                ent.UserLastName= dr["userLastName"].ToString();
                ent.UserAdress = dr["userAdress"].ToString();
                ent.UserEmail = dr["userEmail"].ToString();
                ent.UserPassword = dr["userPassword"].ToString();
                UserList.Add(ent);
            }
            dr.Close();
            return UserList;
        }
        /// <summary> 
        /// Add user to the User Database
        /// </summary>
        /// <param name="user">User object which holds user information</param>
        /// <returns>
        /// the number of how many data are added to the data
        /// </returns>
        public static int UserAdd(EntityUser user)
        {
            SqlCommand komut = new SqlCommand("insert into UserDataBase (userName,userFirstName,userLastName,userAdress,userEmail,userPassword) VALUES (@P1,@P2,@P3,@P4,@P5,@P6)", Baglanti.GetInstance().bgl);
            if (komut.Connection.State != ConnectionState.Open)
                komut.Connection.Open();
            komut.Parameters.AddWithValue("@P1", user.UserName);
            komut.Parameters.AddWithValue("@P2", user.UserFirstName);
            komut.Parameters.AddWithValue("@P3", user.UserLastName);
            komut.Parameters.AddWithValue("@P4", user.UserAdress);
            komut.Parameters.AddWithValue("@P5", user.UserEmail);
            komut.Parameters.AddWithValue("@P6", user.UserPassword);
            return komut.ExecuteNonQuery();
        }

        /// <summary> 
        /// Update user from User Database
        /// </summary>
        /// <param name="user">User object which holds user information</param>
        /// <returns>
        /// information about product has been updated or not
        /// </returns>
        public static bool userUpdate(EntityUser user)
        {
            SqlCommand komut3 = new SqlCommand("Update UserDataBase SET userAdress=@P1,userEmail=@P2,userPassword=@P3 WHERE userId=@p4", Baglanti.GetInstance().bgl);
            if (komut3.Connection.State != ConnectionState.Open)
                komut3.Connection.Open();
            komut3.Parameters.AddWithValue("@P1", user.UserAdress);
            komut3.Parameters.AddWithValue("@P2", user.UserEmail);
            komut3.Parameters.AddWithValue("@P3", user.UserPassword);
            komut3.Parameters.AddWithValue("@P4", user.UserId);
            return komut3.ExecuteNonQuery() > 0;
        }
    }
}
