﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityStore;
using System.Data.SqlClient;
using System.Data;
namespace DTLStore
{
    /*! 
    *  \brief     This is a data access class.  
    *  \details   This class allows us to access the Books in the database.
    *  \author    MEHMET ÇAKAR 152120181023
    *  \date      01.06.2021
    */
    public class DALBook
    {
        /// <summary> 
        /// Add all Books in the database to the EntityBook list
        /// </summary>
        /// <returns>
        /// returns a list of all MusicCDs in the database
        /// </returns>
        public static List<EntityBook> BookList()
        {
            Baglanti baglanti = Baglanti.GetInstance();
            List<EntityBook> Books = new List<EntityBook>();
            SqlCommand komut1 = new SqlCommand("Select * from ProductBook", baglanti.bgl);
            if (komut1.Connection.State!=ConnectionState.Open)            
                komut1.Connection.Open();
            SqlDataReader dr = komut1.ExecuteReader();
            while (dr.Read())
            {
                EntityBookFactory factory = new EntityBookFactory();
                EntityBook ent = (EntityBook)factory.ProduceProduct();
                ent.ProductId = int.Parse(dr["productId"].ToString());
                ent.ProductName=dr["productName"].ToString();
                ent.ProductPrice=float.Parse(dr["ProductPrice"].ToString());
                ent.ProductAuthor=dr["ProductAuthor"].ToString();
                ent.ProductISBNNumber=dr["ProductISBNNumber"].ToString();
                ent.ProductPublisher=dr["ProductPublisher"].ToString();
                ent.ProductImageName=dr["ProductImageName"].ToString();
                ent.ProductPage=int.Parse(dr["ProductPage"].ToString());
                Books.Add(ent);
            }
            dr.Close();
            return Books;
        }
    }
}
