﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityStore;
using System.Data.SqlClient;
using System.Data;
namespace DTLStore
{
    /*! 
    *  \brief     This is a data access class.  
    *  \details   This class allows us to access the MusicCDs in the database.
    *  \author    Kadirhan Kubat
    *  \date      03.06.2021
    */
    public class DALMusicCd
    {
        /// <summary> 
        /// Add all Music CDs in the database to the EntityMusicCd list
        /// </summary>
        /// <returns>
        /// returns a list of all MusicCDs in the database
        /// </returns>
        public static List<EntityMusicCd> MusicCdList()
        {
            Baglanti baglanti = Baglanti.GetInstance();
            List<EntityMusicCd> MusicCds = new List<EntityMusicCd>();
            SqlCommand komut3 = new SqlCommand("Select * from ProductMusicCd", baglanti.bgl);
            if (komut3.Connection.State != ConnectionState.Open)
                komut3.Connection.Open();
            SqlDataReader dr = komut3.ExecuteReader();
            while (dr.Read())
            {
                EntityMusicCdFactory factory = new EntityMusicCdFactory();
                EntityMusicCd ent = (EntityMusicCd)factory.ProduceProduct();
                ent.ProductId = int.Parse(dr["productId"].ToString());
                ent.ProductName = dr["productName"].ToString();
                ent.ProductImageName = dr["ProductImageName"].ToString();
                ent.ProductPrice = float.Parse(dr["ProductPrice"].ToString());
                ent.ProductSinger = dr["ProductSinger"].ToString();
                ent.ProductType = dr["ProductType"].ToString();
                MusicCds.Add(ent);
            }
            dr.Close();
            return MusicCds;
        }
    }
}
