﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using  EntityStore;
using System.Data.SqlClient;
using System.Data;
namespace DTLStore
{
    /*! 
    *  \brief     This is a data access class.  
    *  \details   This class allows us to access the Magazines in the database.
    *  \author    Alperen Bişkin
    *  \date      02.06.2021
    */
    public class DALMagazine
    {
        /// <summary> 
        /// Add all Magazines in the database to the EntityMagazine list
        /// </summary>
        /// <returns>
        /// returns a list of all Magazines in the database
        /// </returns>
        public static List<EntityMagazine> MagazineList()
        {
            Baglanti baglanti = Baglanti.GetInstance();
            List<EntityMagazine> Magazines = new List<EntityMagazine>();
            SqlCommand komut2 = new SqlCommand("Select * from ProductMagazine", baglanti.bgl);
            if (komut2.Connection.State != ConnectionState.Open)
                komut2.Connection.Open();
            SqlDataReader dr = komut2.ExecuteReader();
            while (dr.Read())
            {
                EntityMagazineFactory factory = new EntityMagazineFactory();
                EntityMagazine ent = (EntityMagazine)factory.ProduceProduct();
                ent.ProductId = int.Parse(dr["productId"].ToString());
                ent.ProductImageName= dr["ProductImageName"].ToString();
                ent.ProductName = dr["productName"].ToString();
                ent.ProductPrice = float.Parse(dr["ProductPrice"].ToString());
                ent.ProductIssue = dr["ProductIssue"].ToString();
                ent.ProductType= dr["ProductType"].ToString();
                Magazines.Add(ent);
            }
            dr.Close();
            return Magazines;
        }
    }
}
