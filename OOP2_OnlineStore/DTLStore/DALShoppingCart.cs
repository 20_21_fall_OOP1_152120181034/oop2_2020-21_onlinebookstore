﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityStore;
using System.Data.SqlClient;
using System.Data;
namespace DTLStore
{
    /*! 
    *  \brief     This is a data access class.  
    *  \details   This class allows us to access the shopping carts in the database.
    *  \author    Furkan Ziya ILIKKAN  
    *  \date      03.06.2021
    */
    public class DALShoppingCart
    {
        /// <summary> 
        /// Add shopping cart in the database to the EntityShoppingCart list and return this list
        /// </summary>
        /// <param name="userId">unique userId in database</param>
        /// <returns>
        /// a list of all shopping carts in the database
        /// </returns>
        public static List<EntityShoppingCart> Cart(int userId)
        {
            Baglanti baglanti = Baglanti.GetInstance();
            List<EntityShoppingCart> Cart = new List<EntityShoppingCart>();
            SqlCommand komut4 = new SqlCommand("Select * from CartDataBase where cart_userId=" + userId, baglanti.bgl) ;
            if (komut4.Connection.State != ConnectionState.Open)
                komut4.Connection.Open();
            SqlDataReader dr = komut4.ExecuteReader();
            while (dr.Read())
            {
                EntityShoppingCart ent = new EntityShoppingCart();
                ent.CustomerId = int.Parse(dr["cart_UserId"].ToString());
                ent.ProductId = int.Parse(dr["cart_ProductId"].ToString());
                ent.ProductQuantity= int.Parse(dr["cart_ProductQuantity"].ToString());
                ent.Cart_ProductTotalPrice = float.Parse(dr["cart_ProductTotalPrice"].ToString());
                ent.Cart_ProductName = dr["cart_ProductName"].ToString();
                Cart.Add(ent);
            }
            dr.Close();
            return Cart;
        }
        /// <summary> 
        /// Add product to user shopping cart in database
        /// </summary>
        /// <param name="productId">unique productId in database</param>
        /// <param name="userId">unique userId in database</param>
        /// <param name="quantity">value of how many products there are</param>
        /// <param name="price">value of product's price</param>
        /// <param name="name">name of product</param>
        /// <returns>
        /// the number of how many value are added to the data
        /// </returns>
        public static int BookAdd(int productId,int userId,int quantity,float price,string name)
        {
            SqlCommand komut = new SqlCommand("insert into CartDataBase (cart_ProductId,cart_UserId,cart_ProductQuantity,cart_ProductTotalPrice,cart_ProductName) VALUES (@P1,@P2,@P3,@P4,@P5)", Baglanti.GetInstance().bgl);
            if (komut.Connection.State != ConnectionState.Open)
                komut.Connection.Open();
            komut.Parameters.AddWithValue("@P1", productId);            
            komut.Parameters.AddWithValue("@P2", userId);            
            komut.Parameters.AddWithValue("@P3", quantity);
            komut.Parameters.AddWithValue("@P4", price);
            komut.Parameters.AddWithValue("@P5", name);
            return komut.ExecuteNonQuery();
        }
        /// <summary> 
        /// Delete product from user shopping cart in database 
        /// </summary>
        /// <param name="productId">unique productId in database</param>
        /// <param name="userId">unique userId in database</param>
        /// <returns>
        /// information about product been deleted or not
        /// </returns>
        public static bool BookDelete(int productId,int userId)
        {
            SqlCommand komut2 = new SqlCommand("Delete from CartDataBase where cart_productId="+productId+" AND cart_userId="+userId,Baglanti.GetInstance().bgl);
            if (komut2.Connection.State != ConnectionState.Open)
                komut2.Connection.Open();
            return komut2.ExecuteNonQuery() > 0;
        }
        /// <summary> 
        /// Update product in user shopping cart in database 
        /// </summary>
        /// <param name="productId">unique productId in database</param>
        /// <param name="userId">unique userId in database</param>
        /// <param name="quantity">value of how many products there are</param>
        /// <param name="price">value of product's price</param>
        /// /// <returns>
        /// information about product has been updated or not
        /// </returns>
        public static bool BookUpdate(int productId, int userId,int quantity,float price)
        {
            SqlCommand komut3 = new SqlCommand("Update CartDataBase SET cart_ProductQuantity =@P1 , cart_ProductTotalPrice =@P2 where cart_productId =@P3  AND cart_userId =@P4 " , Baglanti.GetInstance().bgl);
            if (komut3.Connection.State != ConnectionState.Open)
                komut3.Connection.Open();
            komut3.Parameters.AddWithValue("@P1", quantity);
            komut3.Parameters.AddWithValue("@P2", price);
            komut3.Parameters.AddWithValue("@P3", productId);
            komut3.Parameters.AddWithValue("@P4", userId);

            return komut3.ExecuteNonQuery() > 0;
        }
    }
}
