﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
namespace DTLStore
{ 
    /*! 
    *  \brief     This is a data connection class.  
    *  \details   This class create using singleton.This help SQL connection.
    *  \author    Sabri Sinan Sağlam
    *  \date      30.05.2021
    */
    public class Baglanti
    {

        private Baglanti(){}
        private static Baglanti baglanti=null;
        public SqlConnection bgl=new SqlConnection(@"Data Source = SQL5097.site4now.net; Initial Catalog = db_a75137_rangerd; User Id = db_a75137_rangerd_admin; Password = aliveli1327");
        /// <summary> 
        /// If Object is null, he create an object and return it otherwise return object directly.
        /// </summary>
        public static Baglanti GetInstance()
        {           
            if (baglanti == null)
            {
                baglanti = new Baglanti();
            } 
            return baglanti;            
        }

    }
}
