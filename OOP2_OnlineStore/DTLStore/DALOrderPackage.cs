﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityStore;
using System.Data.SqlClient;
using System.Data;
namespace DTLStore
{
    /*! 
    *  \brief     This is a data access class.  
    *  \details   This class allows us to access the Magazines in the database.
    *  \author    Alperen Bişkin
    *  \date      03.06.2021
    */
    public class DALOrderPackage
    {
        /// <summary> 
        /// Add all OrderPackage in the database to the EntityOrderPackage list
        /// </summary>
        /// <returns>
        /// returns a list of all Magazines in the database
        /// </returns>
        public static List<EntityOrderPackage> OrderPackageList(int userId)
        {
            Baglanti baglanti = Baglanti.GetInstance();
            List<EntityOrderPackage> oPackage = new List<EntityOrderPackage>();
            SqlCommand komut = new SqlCommand("Select * from orderPackageDataBase where orderPackage_userId="+userId, baglanti.bgl);
            if (komut.Connection.State != ConnectionState.Open)
                komut.Connection.Open();
            SqlDataReader dr = komut.ExecuteReader();
            while (dr.Read())
            {
                EntityOrderPackage ent = new EntityOrderPackage();
                ent.OrderPackage_orderId = int.Parse(dr["orderPackage_orderId"].ToString());
                ent.OrderPackage_userId = int.Parse(dr["orderPackage_userId"].ToString());
                oPackage.Add(ent);
            }
            dr.Close();
            return oPackage;
        }
        public static int OrderPackageAdd(int userId)
        {
            SqlCommand komut = new SqlCommand("insert into orderPackageDataBase (orderPackage_userId) VALUES (@P1)", Baglanti.GetInstance().bgl);
            if (komut.Connection.State != ConnectionState.Open)
                komut.Connection.Open();
            komut.Parameters.AddWithValue("@P1", userId);
            return komut.ExecuteNonQuery();
        }
        public static bool OrderPackageDelete(int orderPackageId)
        {
            SqlCommand komut2 = new SqlCommand("Delete from orderPackageDataBase where orderPackage_orderId=" + orderPackageId, Baglanti.GetInstance().bgl);
            if (komut2.Connection.State != ConnectionState.Open)
                komut2.Connection.Open();
            return komut2.ExecuteNonQuery() > 0;
        }
    }
}
