﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityStore;
using System.Data.SqlClient;
using System.Data;
namespace DTLStore
{
    /*! 
    *  \brief     This is a data access class.  
    *  \details   This class allows us to access the orders in the database.
    *  \author    Yakupcan ERGEN
    *  \date      02.06.2021
    */
    public class DALOrderDB
    {
        /// <summary> 
        /// Add all orders to the EntityOrderlist
        /// </summary>
        /// <param name="userId">unique userId in database</param>
        /// <returns>
        /// a list of all Orders in the database
        /// </returns>
        public static List<EntityOrderDB> OrderList(int userId)
        {
            Baglanti baglanti = Baglanti.GetInstance();
            List<EntityOrderDB> Order = new List<EntityOrderDB>();
            SqlCommand komut4 = new SqlCommand("Select * from OrderDataBase where order_userId=" + userId, baglanti.bgl);
            if (komut4.Connection.State != ConnectionState.Open)
                komut4.Connection.Open();
            SqlDataReader dr = komut4.ExecuteReader();
            while (dr.Read())
            {
                EntityOrderDB ent = new EntityOrderDB();
                ent.CustomerId = int.Parse(dr["order_UserId"].ToString());
                ent.ProductId = int.Parse(dr["order_ProductId"].ToString());
                ent.ProductQuantity = int.Parse(dr["order_ProductQuantity"].ToString());
                ent.Cart_ProductTotalPrice = float.Parse(dr["order_ProductTotalPrice"].ToString());
                ent.Cart_ProductName = dr["order_ProductName"].ToString();
                ent.DateTime = Convert.ToDateTime(dr["order_Time"].ToString());
                ent.Order_PackageId = int.Parse(dr["order_PackageId"].ToString());
                Order.Add(ent);
            }
            dr.Close();
            return Order;
        }
        /// <summary> 
        /// Add order to ordertable in database
        /// </summary>
        /// <param name="productId">unique productId in database</param>
        /// <param name="userId">unique userId in database</param>
        /// <param name="quantity">value of how many products there are</param>
        /// <param name="price">value of product's price</param>
        /// <param name="name">name of product</param>
        /// <param name="dateTime">the date the order was created</param>
        /// <returns>
        /// the number of how many data are added to the data
        /// </returns>
        public static int OrderAdd(int productId, int userId, int quantity, float price, string name,DateTime dateTime,int PackageId)
        {
            SqlCommand komut = new SqlCommand("insert into OrderDataBase (order_ProductId,order_UserId,order_ProductQuantity,order_ProductTotalPrice,order_ProductName,order_Time,order_PackageId) VALUES (@P1,@P2,@P3,@P4,@P5,@P6,@P7)", Baglanti.GetInstance().bgl);
            if (komut.Connection.State != ConnectionState.Open)
                komut.Connection.Open();
            komut.Parameters.AddWithValue("@P1", productId);
            komut.Parameters.AddWithValue("@P2", userId);
            komut.Parameters.AddWithValue("@P3", quantity);
            komut.Parameters.AddWithValue("@P4", price);
            komut.Parameters.AddWithValue("@P5", name);
            komut.Parameters.AddWithValue("@P6", dateTime);
            komut.Parameters.AddWithValue("@P7", PackageId);
            return komut.ExecuteNonQuery();
        }
        /// <summary> 
        /// Delete products from user order in database 
        /// </summary>
        /// <param name="productId">unique productId in database</param>
        /// <param name="userId">unique userId in database</param>
        /// <returns>
        /// information about product has been deleted or not
        /// </returns>
        public static bool OrderDelete(int productId, int userId)
        {
            SqlCommand komut2 = new SqlCommand("Delete from OrderDataBase where order_productId=" + productId + " AND order_userId=" + userId, Baglanti.GetInstance().bgl);
            if (komut2.Connection.State != ConnectionState.Open)
                komut2.Connection.Open();
            return komut2.ExecuteNonQuery() > 0;
        }       
    }
}
