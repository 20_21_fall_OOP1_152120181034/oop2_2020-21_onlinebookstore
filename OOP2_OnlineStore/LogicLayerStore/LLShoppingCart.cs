﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityStore;
using DTLStore;
namespace LogicLayerStore
{
    public class LLShoppingCart
    {
        public static List<EntityShoppingCart> LLShoppingCartList(int userID)
        {
            return DALShoppingCart.Cart(userID);
        }
        public static int LLBookAdd(int productId,int userId,int quantity,float price,string name)
        {
            return DALShoppingCart.BookAdd(productId, userId, quantity,price,name);
        }
        public static bool LLBookDelete(int productId, int userId)
        {
            return DALShoppingCart.BookDelete(productId, userId);
        }
        public static bool BookUpdate(int productId, int userId, int new_quantity, float new_price)
        {
            return DALShoppingCart.BookUpdate(productId, userId, new_quantity, new_price);
        }
    }
}
