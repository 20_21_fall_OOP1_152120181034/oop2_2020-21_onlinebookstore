﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityStore;
using DTLStore;
namespace LogicLayerStore
{
    public class LLMusicCd
    {
        public static List<EntityMusicCd> LLMusicCdList()
        {
            return DALMusicCd.MusicCdList();
        }
    }
}
