﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityStore;
using DTLStore;
namespace LogicLayerStore
{
    public class LLOrderDB
    {
        public static List<EntityOrderDB> LLOrderDBList(int userID)
        {
            return DALOrderDB.OrderList(userID);
        }
        public static int LLOrderAdd(int productId, int userId, int quantity, float price, string name,DateTime dateTime,int PackageId)
        {
            return DALOrderDB.OrderAdd(productId, userId, quantity, price, name,dateTime, PackageId);
        }
        public static bool LLOrderDelete(int productId, int userId)
        {
            return DALOrderDB.OrderDelete(productId, userId);
        }
    }
}
