﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityStore;
using DTLStore;

namespace LogicLayerStore
{
    public class LLUserLog
    {
        public static List<EntityUserLog> userLogList(int userId)
        {
            return DALUserLog.userLogList(userId);
        }
        public static int UserLogAdd(EntityUserLog userLog)
        {
            return DALUserLog.UserLogAdd(userLog);
        }
    }
}
