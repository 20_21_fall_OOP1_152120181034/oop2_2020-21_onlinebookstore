﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityStore;
using DTLStore;
namespace LogicLayerStore
{
    public class LLMagazine
    {
        public static List<EntityMagazine> LLMagazineList()
        {
            return DALMagazine.MagazineList();
        }
    }
}
