﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityStore;
using DTLStore;
namespace LogicLayerStore
{
    public class LLOrderPackage
    {
        public static List<EntityOrderPackage> LLOrderPackageList(int userId)
        {
            return DALOrderPackage.OrderPackageList(userId);
        }
        public static int LLOrderPackageAdd(int userId)
        {
            return DALOrderPackage.OrderPackageAdd(userId);
        }
        public static bool LLOrderPackageDelete(int orderPackageId)
        {
            return DALOrderPackage.OrderPackageDelete(orderPackageId);
        }
    }
}
