﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityStore;
using DTLStore;
namespace LogicLayerStore
{
    public class LLBook
    {
        public static List<EntityBook> LLBookList()
        {
            return DALBook.BookList();
        }
    }
}
