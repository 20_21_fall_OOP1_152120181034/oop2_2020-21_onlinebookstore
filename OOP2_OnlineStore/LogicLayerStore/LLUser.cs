﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityStore;
using DTLStore;

namespace LogicLayerStore
{
    public class LLUser
    {
        public static List<EntityUser> LLUserList()
        {
            return DALUser.UserList();
        }
        public static int LLUserAdd(EntityUser user)
        {
            return DALUser.UserAdd(user);
        }
        public static bool LLUserUpdate(EntityUser user)
        {
            return DALUser.userUpdate(user);
        }
    }
}
