﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityStore
{
    /*! 
    *  \brief     This is a data access class.  
    *  \details   This class allows us to access the MusicCDs in the database.
    *  \author    Kadirhan Kubat
    *  \date      03.06.2021
    */
    public class EntityMusicCd : EntityProduct
    {
        string productSinger;
        string productType;

        public int ProductId { get => productId; set => productId = value; }
        public string ProductName { get => productName; set => productName = value; }
        public float ProductPrice { get => productPrice; set => productPrice = value; }
        public string ProductImageName { get => productImageName; set => productImageName = value; }
        public string ProductSinger { get => productSinger; set => productSinger = value; }
        public string ProductType { get => productType; set => productType = value; }

       
    }
}
