﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityStore
{
    /*! 
    *  \brief     This is a data access class.  
    *  \details   This class allows us to access the users in the database
    *  \author    Yakupcan ERGEN
    *  \date      03.06.2021
    */
    public class EntityUserLog
    {
        int customerId;
        string customerName;
        string customerEvent;
        DateTime customerEventTimeStampt;

        public int CustomerId { get => customerId; set => customerId = value; }
        public string CustomerEvent { get => customerEvent; set => customerEvent = value; }
        public DateTime CustomerEventTimeStampt { get => customerEventTimeStampt; set => customerEventTimeStampt = value; }
        public string CustomerName { get => customerName; set => customerName = value; }
    }
}
