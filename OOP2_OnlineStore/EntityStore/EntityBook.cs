﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityStore
{
    /*! 
    *  \brief     This is a data access class.  
    *  \details   This class allows us to access the Books in the database.
    *  \author    MEHMET ÇAKAR 152120181023
    *  \date      01.06.2021
    */
    public class EntityBook : EntityProduct
    {
        string productISBNNumber;
        string productAuthor;
        string productPublisher;
        int productPage;

        public int ProductId { get => productId; set => productId = value; }
        public string ProductName { get => productName; set => productName = value; }
        public float ProductPrice { get => productPrice; set => productPrice = value; }
        public string ProductImageName { get => productImageName; set => productImageName = value; }
        public string ProductISBNNumber { get => productISBNNumber; set => productISBNNumber = value; }
        public string ProductAuthor { get => productAuthor; set => productAuthor = value; }
        public string ProductPublisher { get => productPublisher; set => productPublisher = value; }
        public int ProductPage { get => productPage; set => productPage = value; }

       
       
    }
}
