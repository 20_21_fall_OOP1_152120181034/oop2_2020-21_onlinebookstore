﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityStore
{
    /*! 
    *  \brief     This is a data access class.  
    *  \details   This class allows us to access the shopping carts in the database.
    *  \author    Furkan Ziya ILIKKAN  
    *  \date      03.06.2021
    */
    public class EntityShoppingCart
    {
        int customerId;
        int productId;
        int productQuantity=0;
        float cart_ProductTotalPrice;
        string cart_ProductName;
        
        public int CustomerId { get => customerId; set => customerId = value; }
        public int ProductId { get => productId; set => productId = value; }
        public int ProductQuantity { get => productQuantity; set => productQuantity = value; }
        public float Cart_ProductTotalPrice { get => cart_ProductTotalPrice; set => cart_ProductTotalPrice = value; }
        public string Cart_ProductName { get => cart_ProductName; set => cart_ProductName = value; }

    }
}
