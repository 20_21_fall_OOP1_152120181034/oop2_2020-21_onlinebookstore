﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityStore
{
    /*! 
    *  \brief     This is a data access class.  
    *  \details   This class allows us to access the Magazines in the database.
    *  \author    Alperen Bişkin
    *  \date      02.06.2021
    */
    public class EntityMagazine : EntityProduct
    {
        string productIssue;
        string productType;

        public int ProductId { get => productId; set => productId = value; }
        public string ProductName { get => productName; set => productName = value; }
        public float ProductPrice { get => productPrice; set => productPrice = value; }
        public string ProductImageName { get => productImageName; set => productImageName = value; }
        public string ProductIssue { get => productIssue; set => productIssue = value; }
        public string ProductType { get => productType; set => productType = value; }

        
    }
}
