﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityStore
{
    /*! 
    *  \brief     This is a data access class.  
    *  \details   This class allows us to access the orders in the database.
    *  \author    Yakupcan ERGEN
    *  \date      02.06.2021
    */
    public class EntityOrderDB
    {
        int customerId;
        int productId;
        int productQuantity = 0;
        float cart_ProductTotalPrice;
        string cart_ProductName;
        DateTime dateTime;
        int order_PackageId;

        public int CustomerId { get => customerId; set => customerId = value; }
        public int ProductId { get => productId; set => productId = value; }
        public int ProductQuantity { get => productQuantity; set => productQuantity = value; }
        public float Cart_ProductTotalPrice { get => cart_ProductTotalPrice; set => cart_ProductTotalPrice = value; }
        public string Cart_ProductName { get => cart_ProductName; set => cart_ProductName = value; }
        public DateTime DateTime { get => dateTime; set => dateTime = value; }
        public int Order_PackageId { get => order_PackageId; set => order_PackageId = value; }
    }
}
