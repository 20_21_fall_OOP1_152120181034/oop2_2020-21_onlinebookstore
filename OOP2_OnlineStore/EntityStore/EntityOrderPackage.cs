﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityStore
{
    /*! 
    *  \brief     This is a data access class.  
    *  \details   This class allows us to access the Magazines in the database.
    *  \author    Alperen Bişkin
    *  \date      03.06.2021
    */
    public class EntityOrderPackage
    {
        int orderPackage_orderId;
        int orderPackage_userId;

        public int OrderPackage_orderId { get => orderPackage_orderId; set => orderPackage_orderId = value; }
        public int OrderPackage_userId { get => orderPackage_userId; set => orderPackage_userId = value; }
    }
}
