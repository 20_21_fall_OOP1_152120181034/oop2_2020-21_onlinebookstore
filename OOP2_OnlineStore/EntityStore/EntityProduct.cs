﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityStore
{
    /*! 
    *  \brief     This is a product class.  
    *  \details   This class keeps product information.
    *  \author    MEHMET ÇAKAR 152120181023
    *  \date      01.06.2021
    */
    public abstract class EntityProduct
    {
        protected int productId;
        protected string productName;
        protected float productPrice;
        protected string productImageName;
    }
}
