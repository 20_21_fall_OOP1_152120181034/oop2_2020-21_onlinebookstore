﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityStore
{
    public class EntityBookFactory:EntityIProductFactory
    {
        /*! 
        *  \brief     This class is Factory Pattern Interface.  
        *  \details   This class allow us to create new Product Factories.
        *  \author    MEHMET ÇAKAR 152120181023
        *  \date      01.06.2021
        */
        public EntityProduct ProduceProduct()
        {
            return new EntityBook();
        }
    }
}
