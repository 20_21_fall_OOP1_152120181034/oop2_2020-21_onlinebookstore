﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityStore
{
    public class EntityMagazineFactory : EntityIProductFactory
    {
        /*! 
        *  \brief     This class is Factory Pattern Interface.  
        *  \details   This class allow us to create new Product Factories.
        *  \author    Alperen Bişkin
        *  \date      02.06.2021
        */
        public EntityProduct ProduceProduct()
        {
            return new EntityMagazine();
        }
    }
}
