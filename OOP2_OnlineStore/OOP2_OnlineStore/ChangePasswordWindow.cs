﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EntityStore;
using DTLStore;
using LogicLayerStore;
namespace OOP_Project
{
    public partial class ChangePasswordWindow : Form
    {
        EntityUser currentUser;
        public ChangePasswordWindow(EntityUser user)
        {
            InitializeComponent();
            currentUser = user;
        }

        private void ConfirmButton_Click(object sender, EventArgs e)
        {
            makeErrorsInvisible();
            bool flag = false;           
            if (txtbxPasswordOne.Text == string.Empty)
            {
                txtbxPasswordOne.LineIdleColor = Color.Red;
                txtbxPasswordOne.LineFocusedColor = Color.Red;
                txtbxPasswordOne.LineMouseHoverColor = Color.Red;
                lblFirstPasswordError.Text = "Password cannot be left blank";
                lblFirstPasswordError.Visible = true;
                flag = true;
            }
            if (txtbxPasswordTwo.Text == string.Empty)
            {
                txtbxPasswordTwo.LineIdleColor = Color.Red;
                txtbxPasswordTwo.LineFocusedColor = Color.Red;
                txtbxPasswordTwo.LineMouseHoverColor = Color.Red;
                lblSecondPasswordError.Text = "Password cannot be left blank";
                lblSecondPasswordError.Visible = true;
                flag = true;
            }
            if (currentUser.encryptPassword(txtbxOldPassword.Text) != currentUser.UserPassword)
            {
                txtbxOldPassword.LineIdleColor = Color.Red;
                txtbxOldPassword.LineFocusedColor = Color.Red;
                txtbxOldPassword.LineMouseHoverColor = Color.Red;
                lblOldPasswordError.Text = "Password is wrong";
                lblOldPasswordError.Visible = true;
            }
            else if (!flag)
            {
                if (txtbxPasswordOne.Text==txtbxPasswordTwo.Text)
                {
                    currentUser.UserPassword = currentUser.encryptPassword(txtbxPasswordOne.Text);
                    LLUser.LLUserUpdate(currentUser);
                    this.Close();
                }
                else
                {
                    lblSecondPasswordError.Text = "Passwords does not match";
                    lblFirstPasswordError.Text = "Passwords does not match";
                    txtbxPasswordTwo.LineIdleColor = Color.Red;
                    txtbxPasswordTwo.LineFocusedColor = Color.Red;
                    txtbxPasswordTwo.LineMouseHoverColor = Color.Red;
                    lblSecondPasswordError.Visible = true;
                    txtbxPasswordOne.LineIdleColor = Color.Red;
                    txtbxPasswordOne.LineFocusedColor = Color.Red;
                    txtbxPasswordOne.LineMouseHoverColor = Color.Red;
                    lblFirstPasswordError.Visible = true;
                }
            }
           
        }
        private void makeErrorsInvisible()
        {
            txtbxPasswordOne.LineIdleColor = Color.Teal;
            txtbxOldPassword.LineIdleColor = Color.Teal;
            txtbxPasswordTwo.LineIdleColor = Color.Teal;
            txtbxPasswordOne.LineFocusedColor = Color.Teal;
            txtbxOldPassword.LineFocusedColor = Color.Teal;
            txtbxPasswordTwo.LineFocusedColor = Color.Teal;
            txtbxPasswordOne.LineMouseHoverColor = Color.Teal;
            txtbxOldPassword.LineMouseHoverColor = Color.Teal;
            txtbxPasswordTwo.LineMouseHoverColor = Color.Teal;
            lblFirstPasswordError.Visible = false;
            lblOldPasswordError.Visible = false;
            lblSecondPasswordError.Visible = false;
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtbxOldPassword_OnValueChanged(object sender, EventArgs e)
        {
            txtbxOldPassword.isPassword = true;
        }

        private void txtbxPasswordOne_OnValueChanged(object sender, EventArgs e)
        {
            txtbxPasswordOne.isPassword = true;
        }

        private void txtbxPasswordTwo_OnValueChanged(object sender, EventArgs e)
        {
            txtbxPasswordTwo.isPassword = true;
        }
    }
}
