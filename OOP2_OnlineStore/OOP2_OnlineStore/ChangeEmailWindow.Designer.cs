﻿
namespace OOP_Project
{
    partial class ChangeEmailWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChangeEmailWindow));
            this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.lblEmailError = new System.Windows.Forms.Label();
            this.txtbxEmail = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtbxReEmail = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.lblReEmailError = new System.Windows.Forms.Label();
            this.CancelButton = new Bunifu.Framework.UI.BunifuImageButton();
            this.ConfirmButton = new Bunifu.Framework.UI.BunifuImageButton();
            ((System.ComponentModel.ISupportInitialize)(this.CancelButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConfirmButton)).BeginInit();
            this.SuspendLayout();
            // 
            // bunifuElipse1
            // 
            this.bunifuElipse1.ElipseRadius = 25;
            this.bunifuElipse1.TargetControl = this;
            // 
            // lblEmailError
            // 
            this.lblEmailError.AutoSize = true;
            this.lblEmailError.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblEmailError.ForeColor = System.Drawing.Color.Red;
            this.lblEmailError.Location = new System.Drawing.Point(116, 44);
            this.lblEmailError.Name = "lblEmailError";
            this.lblEmailError.Size = new System.Drawing.Size(36, 17);
            this.lblEmailError.TabIndex = 12;
            this.lblEmailError.Text = "Error";
            this.lblEmailError.Visible = false;
            // 
            // txtbxEmail
            // 
            this.txtbxEmail.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txtbxEmail.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtbxEmail.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtbxEmail.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtbxEmail.HintForeColor = System.Drawing.Color.Empty;
            this.txtbxEmail.HintText = "someone@example.com";
            this.txtbxEmail.isPassword = false;
            this.txtbxEmail.LineFocusedColor = System.Drawing.Color.Teal;
            this.txtbxEmail.LineIdleColor = System.Drawing.Color.Teal;
            this.txtbxEmail.LineMouseHoverColor = System.Drawing.Color.Teal;
            this.txtbxEmail.LineThickness = 3;
            this.txtbxEmail.Location = new System.Drawing.Point(27, 58);
            this.txtbxEmail.Margin = new System.Windows.Forms.Padding(4);
            this.txtbxEmail.Name = "txtbxEmail";
            this.txtbxEmail.Size = new System.Drawing.Size(297, 31);
            this.txtbxEmail.TabIndex = 11;
            this.txtbxEmail.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Teal;
            this.label6.Location = new System.Drawing.Point(23, 41);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 21);
            this.label6.TabIndex = 10;
            this.label6.Text = "New Email:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Teal;
            this.label1.Location = new System.Drawing.Point(23, 113);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 21);
            this.label1.TabIndex = 10;
            this.label1.Text = "Re-Email:";
            // 
            // txtbxReEmail
            // 
            this.txtbxReEmail.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txtbxReEmail.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtbxReEmail.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtbxReEmail.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtbxReEmail.HintForeColor = System.Drawing.Color.Empty;
            this.txtbxReEmail.HintText = "someone@example.com";
            this.txtbxReEmail.isPassword = false;
            this.txtbxReEmail.LineFocusedColor = System.Drawing.Color.Teal;
            this.txtbxReEmail.LineIdleColor = System.Drawing.Color.Teal;
            this.txtbxReEmail.LineMouseHoverColor = System.Drawing.Color.Teal;
            this.txtbxReEmail.LineThickness = 3;
            this.txtbxReEmail.Location = new System.Drawing.Point(27, 130);
            this.txtbxReEmail.Margin = new System.Windows.Forms.Padding(4);
            this.txtbxReEmail.Name = "txtbxReEmail";
            this.txtbxReEmail.Size = new System.Drawing.Size(297, 31);
            this.txtbxReEmail.TabIndex = 11;
            this.txtbxReEmail.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // lblReEmailError
            // 
            this.lblReEmailError.AutoSize = true;
            this.lblReEmailError.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblReEmailError.ForeColor = System.Drawing.Color.Red;
            this.lblReEmailError.Location = new System.Drawing.Point(100, 116);
            this.lblReEmailError.Name = "lblReEmailError";
            this.lblReEmailError.Size = new System.Drawing.Size(36, 17);
            this.lblReEmailError.TabIndex = 12;
            this.lblReEmailError.Text = "Error";
            this.lblReEmailError.Visible = false;
            // 
            // CancelButton
            // 
            this.CancelButton.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.CancelButton.Image = ((System.Drawing.Image)(resources.GetObject("CancelButton.Image")));
            this.CancelButton.ImageActive = null;
            this.CancelButton.Location = new System.Drawing.Point(202, 236);
            this.CancelButton.Margin = new System.Windows.Forms.Padding(2);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(45, 49);
            this.CancelButton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.CancelButton.TabIndex = 22;
            this.CancelButton.TabStop = false;
            this.CancelButton.Zoom = 10;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // ConfirmButton
            // 
            this.ConfirmButton.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.ConfirmButton.Image = ((System.Drawing.Image)(resources.GetObject("ConfirmButton.Image")));
            this.ConfirmButton.ImageActive = null;
            this.ConfirmButton.Location = new System.Drawing.Point(96, 236);
            this.ConfirmButton.Margin = new System.Windows.Forms.Padding(2);
            this.ConfirmButton.Name = "ConfirmButton";
            this.ConfirmButton.Size = new System.Drawing.Size(45, 49);
            this.ConfirmButton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ConfirmButton.TabIndex = 21;
            this.ConfirmButton.TabStop = false;
            this.ConfirmButton.Zoom = 10;
            this.ConfirmButton.Click += new System.EventHandler(this.ConfirmButton_Click);
            // 
            // ChangeEmailWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.ClientSize = new System.Drawing.Size(357, 305);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.ConfirmButton);
            this.Controls.Add(this.lblReEmailError);
            this.Controls.Add(this.lblEmailError);
            this.Controls.Add(this.txtbxReEmail);
            this.Controls.Add(this.txtbxEmail);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label6);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ChangeEmailWindow";
            this.Text = "ChangeEmailWindows";
            ((System.ComponentModel.ISupportInitialize)(this.CancelButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConfirmButton)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
        private System.Windows.Forms.Label lblReEmailError;
        private System.Windows.Forms.Label lblEmailError;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtbxReEmail;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtbxEmail;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private Bunifu.Framework.UI.BunifuImageButton CancelButton;
        public Bunifu.Framework.UI.BunifuImageButton ConfirmButton;
    }
}