﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicLayerStore;
using EntityStore;
using DTLStore;
namespace OOP_Project
{
    /*! 
    *  \brief     This is a data access class.  
    *  \details   This class allows us to access the Magazines in the database.
    *  \author    Alperen Bişkin
    *  \date      04.06.2021
    */
    public partial class SignUpForm : Form
    {
        List<EntityUser> users = LLUser.LLUserList();
        public SignUpForm()
        {
            InitializeComponent();
            makeErrorsInvisible();
        }
        /// <summary> 
        /// Exit button
        /// </summary>
        private void lblExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        /// <summary> 
        /// Show the LoginForm
        /// </summary>
        private void lblSignIn_Click(object sender, EventArgs e)
        {
            LoginForm loginForm = new LoginForm();
            this.Hide();
            loginForm.ShowDialog();
            this.Close();

        }

        private void lblSignIn_MouseHover(object sender, EventArgs e)
        {
            lblSignIn.ForeColor = Color.FromArgb(0, 64, 64);
        }

        private void lblSignIn_MouseLeave(object sender, EventArgs e)
        {
            lblSignIn.ForeColor = Color.Teal;
        }
        /// <summary> 
        /// Check the all user information's availability in textboxes
        /// If all information's availability are okay, add the new user to DataBase
        /// </summary>
        private void btnSignUp_Click(object sender, EventArgs e)
        {
            makeErrorsInvisible();
            EntityUser user = new EntityUser();
            bool flag = false;
            bool flagPW = false;
            if (txtbxUserName.Text == string.Empty)
            {
                txtbxUserName.LineIdleColor = Color.Red;
                txtbxUserName.LineFocusedColor = Color.Red;
                txtbxUserName.LineMouseHoverColor = Color.Red;
                lblUserNameError.Text = "User name cannot be left blank";
                lblUserNameError.Visible = true;
                flag = true;
            }

            else
                foreach (var item in users)
                {
                    if (item.UserName == txtbxUserName.Text)
                    {
                        txtbxUserName.LineIdleColor = Color.Red;
                        txtbxUserName.LineFocusedColor = Color.Red;
                        txtbxUserName.LineMouseHoverColor = Color.Red;
                        lblUserNameError.Text = "This user name already exist";
                        lblUserNameError.Visible = true;
                        flag = true;
                        break;
                    }
                }
            if (txtbxFirstName.Text == string.Empty)
            {
                txtbxFirstName.LineIdleColor = Color.Red;
                txtbxFirstName.LineFocusedColor = Color.Red;
                txtbxFirstName.LineMouseHoverColor = Color.Red;
                lblFirstNameError.Text = "First name cannot be left blank";
                lblFirstNameError.Visible = true;
                flag = true;

            }
            if (txtbxLastName.Text == string.Empty)
            {
                txtbxLastName.LineIdleColor = Color.Red;
                txtbxLastName.LineFocusedColor = Color.Red;
                txtbxLastName.LineMouseHoverColor = Color.Red;
                lblLastNameError.Text = "Last name cannot be left blank";
                lblLastNameError.Visible = true;
                flag = true;
            }
            if (txtbxPasswordOne.Text == string.Empty)
            {
                txtbxPasswordOne.LineIdleColor = Color.Red;
                txtbxPasswordOne.LineFocusedColor = Color.Red;
                txtbxPasswordOne.LineMouseHoverColor = Color.Red;
                lblFirstPasswordError.Text = "Password cannot be left blank";
                lblFirstPasswordError.Visible = true;
                flag = true;
                flagPW = true;
            }
            if (txtbxPasswordTwo.Text == string.Empty)
            {
                txtbxPasswordTwo.LineIdleColor = Color.Red;
                txtbxPasswordTwo.LineFocusedColor = Color.Red;
                txtbxPasswordTwo.LineMouseHoverColor = Color.Red;
                lblSecondPasswordError.Text = "Password cannot be left blank";
                lblSecondPasswordError.Visible = true;
                flag = true;
                flagPW = true;
            }
            if (!flagPW)
            {
                if (txtbxPasswordOne.Text != txtbxPasswordTwo.Text)
                {
                    txtbxPasswordOne.LineIdleColor = Color.Red;
                    txtbxPasswordOne.LineFocusedColor = Color.Red;
                    txtbxPasswordOne.LineMouseHoverColor = Color.Red;
                    lblFirstPasswordError.Text = "Passwords do not match";
                    lblFirstPasswordError.Visible = true;
                    txtbxPasswordTwo.LineIdleColor = Color.Red;
                    txtbxPasswordTwo.LineFocusedColor = Color.Red;
                    txtbxPasswordTwo.LineMouseHoverColor = Color.Red;
                    lblSecondPasswordError.Text = "Passwords do not match";
                    lblSecondPasswordError.Visible = true;
                    flag = true;
                }
            }
            if (txtbxEmail.Text == string.Empty)
            {
                txtbxEmail.LineIdleColor = Color.Red;
                txtbxEmail.LineFocusedColor = Color.Red;
                txtbxEmail.LineMouseHoverColor = Color.Red;
                lblEmailError.Text = "Email cannot be left blank";
                lblEmailError.Visible = true;
                flag = true;
            }
            if (txtbxAdress.Text == string.Empty)
            {
                txtbxAdress.LineIdleColor = Color.Red;
                txtbxAdress.LineFocusedColor = Color.Red;
                txtbxAdress.LineMouseHoverColor = Color.Red;

                lblAdressError.Text = "Adress cannot be left blank";
                lblAdressError.Visible = true;
                flag = true;
            }
            if (chckbxAgree.Checked == false)
            {
                lblchckBoxError.Text = "You must confirm the terms and conditions";
                lblchckBoxError.Visible = true;
            }
            if (!flag)
            {
                user.UserName = txtbxUserName.Text;
                user.UserFirstName = txtbxFirstName.Text;
                user.UserLastName = txtbxLastName.Text;
                user.UserAdress = txtbxAdress.Text;
                user.UserEmail = txtbxEmail.Text;
                user.UserPassword = user.encryptPassword(txtbxPasswordOne.Text);
                if (LLUser.LLUserAdd(user) != -1)
                {
                    lblSignUpMessage.Visible = true;
                    lblSignUpMessage.Show();
                    btnSignUp.Enabled = false;

                }
            }
        }
        /// <summary> 
        /// Visual improvemenets
        /// </summary>
        private void makeErrorsInvisible()
        {
            txtbxUserName.LineIdleColor = Color.Teal;
            txtbxFirstName.LineIdleColor = Color.Teal;
            txtbxLastName.LineIdleColor = Color.Teal;
            txtbxPasswordOne.LineIdleColor = Color.Teal;
            txtbxPasswordTwo.LineIdleColor = Color.Teal;
            txtbxEmail.LineIdleColor = Color.Teal;
            txtbxAdress.LineIdleColor = Color.Teal;
            txtbxUserName.LineFocusedColor = Color.Teal;
            txtbxFirstName.LineFocusedColor = Color.Teal;
            txtbxLastName.LineFocusedColor = Color.Teal;
            txtbxPasswordOne.LineFocusedColor = Color.Teal;
            txtbxPasswordTwo.LineFocusedColor = Color.Teal;
            txtbxEmail.LineFocusedColor = Color.Teal;
            txtbxAdress.LineFocusedColor = Color.Teal;
            txtbxUserName.LineMouseHoverColor = Color.Teal;
            txtbxFirstName.LineMouseHoverColor = Color.Teal;
            txtbxLastName.LineMouseHoverColor = Color.Teal;
            txtbxPasswordOne.LineMouseHoverColor = Color.Teal;
            txtbxPasswordTwo.LineMouseHoverColor = Color.Teal;
            txtbxEmail.LineMouseHoverColor = Color.Teal;
            txtbxAdress.LineMouseHoverColor = Color.Teal;
            lblUserNameError.Visible = false;
            lblFirstNameError.Visible = false;
            lblLastNameError.Visible = false;
            lblFirstPasswordError.Visible = false;
            lblSecondPasswordError.Visible = false;
            lblEmailError.Visible = false;
            lblAdressError.Visible = false;
            lblchckBoxError.Visible = false;
        }
        
        private void txtbxPasswordOne_OnValueChanged(object sender, EventArgs e)
        {
            txtbxPasswordOne.isPassword = true;
        }

        private void txtbxPasswordTwo_OnValueChanged(object sender, EventArgs e)
        {
            txtbxPasswordTwo.isPassword = true;
        }

    }
}
