﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EntityStore;
using DTLStore;
using LogicLayerStore;

namespace OOP_Project
{
    public partial class DashBoardForm : Form
    {
        private Panel currenButtonPanel;
        private int currentUserID;
        private string currentUserName;
        public DashBoardForm(int id)
        {
            InitializeComponent();
            this.currentUserID = id;
        }
        /// <summary> 
        /// When form opened, necessary values assign
        /// </summary>
        private void DashBoardForm_Load(object sender, EventArgs e)
        {
            List<EntityUser> Users = LLUser.LLUserList();
            foreach (var user in Users)
                if (user.UserId == currentUserID)
                {
                    lblUserName.Text = user.UserName;
                    currentUserName = user.UserName;
                }
            sendLog("User loginned in successfully");
            currenButtonPanel = homeButtonPanel;
            activatePanel(homeButtonPanel);
            this.panelSag.Controls.Clear();
            HomePageForm frm = new HomePageForm(this.currentUserID, this.currentUserName);
            frm.TopLevel = false;
            frm.AutoScroll = true;
            this.panelSag.Controls.Add(frm);
            frm.Show();
            
        }
        /// <summary> 
        /// Home page button
        /// </summary>
        private void btnHome_Click(object sender, EventArgs e)
        {
            if (currenButtonPanel != homeButtonPanel)
            {
                activatePanel(homeButtonPanel);
                this.panelSag.Controls.Clear();
                HomePageForm frm = new HomePageForm(this.currentUserID, this.currentUserName);
                frm.TopLevel = false;
                frm.AutoScroll = true;
                this.panelSag.Controls.Add(frm);
                frm.Show();
                sendLog("User visited Home Page");
            }
        }
        /// <summary> 
        /// Shopping page button
        /// </summary>
        private void btnShopping_Click(object sender, EventArgs e)
        {
            if (currenButtonPanel != shoppingButtonPanel)
            {
                activatePanel(shoppingButtonPanel);
                this.panelSag.Controls.Clear();
                ShoppingForm frm = new ShoppingForm(this.currentUserID, this.currentUserName);
                frm.TopLevel = false;
                frm.AutoScroll = true;
                this.panelSag.Controls.Add(frm);
                frm.OrderButton.Click += delegate {
                    if (frm.lstviewCart.Items.Count>0)
                    {
                        btnOrders_Click(sender, e);
                        frm.lstviewCart.Items.Clear();
                    }
                };
                frm.Show();
                sendLog("User visited Shopping Page");
            }
        }
        /// <summary> 
        /// Order page button
        /// </summary>
        private void btnOrders_Click(object sender, EventArgs e)
        {
            if (currenButtonPanel != ordersButtonPanel)
            {
                activatePanel(ordersButtonPanel);
                this.panelSag.Controls.Clear();
                OrdersForm frm = new OrdersForm(this.currentUserID, this.currentUserName,this);
                frm.TopLevel = false;
                frm.AutoScroll = true;
                this.panelSag.Controls.Add(frm);
                frm.Show();
                sendLog("User visited Orders Page");
            }
        }
        /// <summary> 
        /// Setting page button
        /// </summary>
        private void btnSettings_Click(object sender, EventArgs e)
        {
            if (currenButtonPanel != settingsButtonPanel)
            {
                activatePanel(settingsButtonPanel);
                this.panelSag.Controls.Clear();
                SettingsForm frm = new SettingsForm(this.currentUserID, this.currentUserName);
                frm.TopLevel = false;
                frm.AutoScroll = true;
                this.panelSag.Controls.Add(frm);
                frm.Show();
                sendLog("User visited Settings Page");
            }
        }
        /// <summary> 
        /// Exit button
        /// </summary>
        private void btnExit_Click(object sender, EventArgs e)
        {
            sendLog("User closed the Online Book Store App");
            Application.Exit();
        }
        /// <summary> 
        /// Log out button
        /// </summary>
        private void btnLogout_Click(object sender, EventArgs e)
        {
            sendLog("User logged out successfully");
            LoginForm loginForm = new LoginForm();
            this.Hide();
            loginForm.ShowDialog();
            this.Close();
        }
        /// <summary> 
        /// Open the menu panel's visibility
        /// </summary>
        private void activatePanel(Panel pnl)
        {
            deactivatePanel();
            pnl.Visible = true;
            currenButtonPanel = pnl;
        }
        /// <summary> 
        /// Close the menu panel's visibility
        /// </summary>
        private void deactivatePanel()
        {
            currenButtonPanel.Visible = false;
        }
        /// <summary> 
        /// Send the users log to database
        /// </summary>
        private void sendLog(string customerEvent)
        {
            EntityUserLog userLog = new EntityUserLog();
            userLog.CustomerId = this.currentUserID;
            userLog.CustomerName = this.currentUserName;
            userLog.CustomerEvent = customerEvent;
            userLog.CustomerEventTimeStampt = DateTime.Now;
            LLUserLog.UserLogAdd(userLog);
        }
    }
}
