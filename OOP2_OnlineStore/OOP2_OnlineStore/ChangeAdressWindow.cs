﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EntityStore;
using DTLStore;
using LogicLayerStore;

namespace OOP_Project
{
    public partial class ChangeAdressWindow : Form
    {
        EntityUser currentUser;
        public ChangeAdressWindow(EntityUser User)
        {
            InitializeComponent();
            currentUser = User;
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ConfirmButton_Click(object sender, EventArgs e)
        {
            txtbxAdress.LineIdleColor = Color.Teal;
            txtbxAdress.LineFocusedColor = Color.Teal;
            txtbxAdress.LineMouseHoverColor = Color.Teal;
            lblAdressError.Visible = false;
            if (txtbxAdress.Text == string.Empty)
            {
                txtbxAdress.LineIdleColor = Color.Red;
                txtbxAdress.LineFocusedColor = Color.Red;
                txtbxAdress.LineMouseHoverColor = Color.Red;

                lblAdressError.Text = "Adress cannot be left blank";
                lblAdressError.Visible = true;
            }
            else
            {
                currentUser.UserAdress = txtbxAdress.Text;
                LLUser.LLUserUpdate(currentUser);
                this.Close();
            }
        }
    }
}
