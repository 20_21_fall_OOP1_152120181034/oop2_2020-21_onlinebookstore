﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EntityStore;
using LogicLayerStore;
using DTLStore;

namespace OOP_Project
{
    public partial class HomePageForm : Form
    {
        private int currentUserID;
        private string currentUserName;
        List<EntityShoppingCart> shopCartList;
        List<EntityOrderDB> orderList;
        public HomePageForm(int currentUserID,string currentUserName)
        {
            InitializeComponent();
            this.currentUserID = currentUserID;
            this.currentUserName = currentUserName;
        }
        /// <summary> 
        /// When form opened, necessary values assign
        /// </summary>
        private void HomePageForm_Load(object sender, EventArgs e)
        {
            shopCartList = LLShoppingCart.LLShoppingCartList(currentUserID);
            orderList = LLOrderDB.LLOrderDBList(currentUserID);
            lblCartCount.Text = shopCartList.Count.ToString();
            lblOrderCount.Text = orderList.Count.ToString();
            int cartBook = 0, cartMagazine = 0, cartMusic = 0;
            int orderBook = 0, orderMagazine = 0, orderMusic = 0;

            // Calculate the user's shopping cart history according to product type
            foreach (var item in shopCartList)
            {
                if (item.ProductId>10000&&item.ProductId<20000)
                {
                    cartBook += item.ProductQuantity;
                }
                else if (item.ProductId > 20000 && item.ProductId < 30000)
                {
                    cartMagazine+= item.ProductQuantity;
                }
                else if (item.ProductId > 30000 && item.ProductId < 40000)
                {
                    cartMusic+= item.ProductQuantity;
                }
            }
            // Calculate the user's order history according to product type
            foreach (var item in orderList)
            {
                if (item.ProductId > 10000 && item.ProductId < 20000)
                {
                    orderBook += item.ProductQuantity;
                }
                else if (item.ProductId > 20000 && item.ProductId < 30000)
                {
                    orderMagazine += item.ProductQuantity;
                }
                else if (item.ProductId > 30000 && item.ProductId < 40000)
                {
                    orderMusic+= item.ProductQuantity;
                }
            }
            // Graphical attachments
            bool cartFlag = false;
            bool orderFlag = false;
            if (cartBook > 0)
            {
                productsPieChart.Series["s1"].Points.AddXY("Books", cartBook);
                cartFlag = true;
            }
            if (cartMusic > 0)
            {
                productsPieChart.Series["s1"].Points.AddXY("MusicCDs", cartMusic);
                cartFlag = true;
            }
            if (cartMagazine > 0)
            {
                productsPieChart.Series["s1"].Points.AddXY("Magazines", cartMagazine);
                cartFlag = true;
            }
            if (orderBook > 0)
            {
                OrdersPieChart.Series["s1"].Points.AddXY("Books", orderBook);
                orderFlag = true;
            }
            if (orderMusic > 0)
            {
                OrdersPieChart.Series["s1"].Points.AddXY("MusicCDs", orderMusic);
                orderFlag = true;
            }
            if (orderMagazine > 0)
            {
                OrdersPieChart.Series["s1"].Points.AddXY("Magazines", orderMagazine);
                orderFlag = true;
            }
            if (!cartFlag)
            {
                productsPieChart.Series["s1"].Points.AddXY("No Item", 1);
            }
            if (!orderFlag)
            {
                OrdersPieChart.Series["s1"].Points.AddXY("No Item", 1);
            }
        }

       
    }
}
