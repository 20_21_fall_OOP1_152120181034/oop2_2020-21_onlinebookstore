﻿
namespace OOP_Project
{
    partial class MusicCDDetailForm
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MusicCDDetailForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.AddToCartButton = new Bunifu.Framework.UI.BunifuFlatButton();
            this.numericValueQuantity = new System.Windows.Forms.NumericUpDown();
            this.bunifuCustomLabel2 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel1 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.MusicPriceLabel = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.MusicTypeLabel = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.MusicSingerLabel = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.MusicNameLabel = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel3 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.lblInfo = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.pctrBoxInfo = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericValueQuantity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctrBoxInfo)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Teal;
            this.panel1.Controls.Add(this.AddToCartButton);
            this.panel1.Controls.Add(this.numericValueQuantity);
            this.panel1.Controls.Add(this.bunifuCustomLabel2);
            this.panel1.Controls.Add(this.bunifuCustomLabel1);
            this.panel1.Controls.Add(this.MusicPriceLabel);
            this.panel1.Controls.Add(this.MusicTypeLabel);
            this.panel1.Controls.Add(this.MusicSingerLabel);
            this.panel1.Controls.Add(this.MusicNameLabel);
            this.panel1.Controls.Add(this.bunifuCustomLabel3);
            this.panel1.Controls.Add(this.lblInfo);
            this.panel1.Controls.Add(this.pctrBoxInfo);
            this.panel1.Location = new System.Drawing.Point(5, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(250, 250);
            this.panel1.TabIndex = 1;
            // 
            // AddToCartButton
            // 
            this.AddToCartButton.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.AddToCartButton.BackColor = System.Drawing.Color.Teal;
            this.AddToCartButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.AddToCartButton.BorderRadius = 0;
            this.AddToCartButton.ButtonText = "Add to Cart";
            this.AddToCartButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.AddToCartButton.DisabledColor = System.Drawing.Color.Gray;
            this.AddToCartButton.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.AddToCartButton.ForeColor = System.Drawing.SystemColors.Control;
            this.AddToCartButton.Iconcolor = System.Drawing.Color.Transparent;
            this.AddToCartButton.Iconimage = ((System.Drawing.Image)(resources.GetObject("AddToCartButton.Iconimage")));
            this.AddToCartButton.Iconimage_right = null;
            this.AddToCartButton.Iconimage_right_Selected = null;
            this.AddToCartButton.Iconimage_Selected = null;
            this.AddToCartButton.IconMarginLeft = 0;
            this.AddToCartButton.IconMarginRight = 0;
            this.AddToCartButton.IconRightVisible = true;
            this.AddToCartButton.IconRightZoom = 0D;
            this.AddToCartButton.IconVisible = true;
            this.AddToCartButton.IconZoom = 60D;
            this.AddToCartButton.IsTab = false;
            this.AddToCartButton.Location = new System.Drawing.Point(95, 209);
            this.AddToCartButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.AddToCartButton.Name = "AddToCartButton";
            this.AddToCartButton.Normalcolor = System.Drawing.Color.Teal;
            this.AddToCartButton.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.AddToCartButton.OnHoverTextColor = System.Drawing.SystemColors.Control;
            this.AddToCartButton.selected = false;
            this.AddToCartButton.Size = new System.Drawing.Size(127, 29);
            this.AddToCartButton.TabIndex = 3;
            this.AddToCartButton.Tag = "AddToCart";
            this.AddToCartButton.Text = "Add to Cart";
            this.AddToCartButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.AddToCartButton.Textcolor = System.Drawing.SystemColors.Control;
            this.AddToCartButton.TextFont = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            // 
            // numericValueQuantity
            // 
            this.numericValueQuantity.BackColor = System.Drawing.Color.Teal;
            this.numericValueQuantity.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.numericValueQuantity.ForeColor = System.Drawing.SystemColors.Control;
            this.numericValueQuantity.Location = new System.Drawing.Point(30, 213);
            this.numericValueQuantity.Margin = new System.Windows.Forms.Padding(2);
            this.numericValueQuantity.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericValueQuantity.Name = "numericValueQuantity";
            this.numericValueQuantity.Size = new System.Drawing.Size(32, 22);
            this.numericValueQuantity.TabIndex = 2;
            this.numericValueQuantity.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // bunifuCustomLabel2
            // 
            this.bunifuCustomLabel2.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.bunifuCustomLabel2.Location = new System.Drawing.Point(14, 169);
            this.bunifuCustomLabel2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.bunifuCustomLabel2.Name = "bunifuCustomLabel2";
            this.bunifuCustomLabel2.Size = new System.Drawing.Size(48, 21);
            this.bunifuCustomLabel2.TabIndex = 1;
            this.bunifuCustomLabel2.Text = "Price:";
            // 
            // bunifuCustomLabel1
            // 
            this.bunifuCustomLabel1.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.bunifuCustomLabel1.Location = new System.Drawing.Point(14, 148);
            this.bunifuCustomLabel1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.bunifuCustomLabel1.Name = "bunifuCustomLabel1";
            this.bunifuCustomLabel1.Size = new System.Drawing.Size(47, 21);
            this.bunifuCustomLabel1.TabIndex = 1;
            this.bunifuCustomLabel1.Text = "Type:";
            // 
            // MusicPriceLabel
            // 
            this.MusicPriceLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.MusicPriceLabel.ForeColor = System.Drawing.SystemColors.Control;
            this.MusicPriceLabel.Location = new System.Drawing.Point(65, 169);
            this.MusicPriceLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.MusicPriceLabel.Name = "MusicPriceLabel";
            this.MusicPriceLabel.Size = new System.Drawing.Size(131, 21);
            this.MusicPriceLabel.TabIndex = 1;
            // 
            // MusicTypeLabel
            // 
            this.MusicTypeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.MusicTypeLabel.ForeColor = System.Drawing.SystemColors.Control;
            this.MusicTypeLabel.Location = new System.Drawing.Point(63, 148);
            this.MusicTypeLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.MusicTypeLabel.Name = "MusicTypeLabel";
            this.MusicTypeLabel.Size = new System.Drawing.Size(136, 21);
            this.MusicTypeLabel.TabIndex = 1;
            // 
            // MusicSingerLabel
            // 
            this.MusicSingerLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.MusicSingerLabel.ForeColor = System.Drawing.SystemColors.Control;
            this.MusicSingerLabel.Location = new System.Drawing.Point(72, 127);
            this.MusicSingerLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.MusicSingerLabel.Name = "MusicSingerLabel";
            this.MusicSingerLabel.Size = new System.Drawing.Size(176, 21);
            this.MusicSingerLabel.TabIndex = 1;
            // 
            // MusicNameLabel
            // 
            this.MusicNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.MusicNameLabel.ForeColor = System.Drawing.SystemColors.Control;
            this.MusicNameLabel.Location = new System.Drawing.Point(112, 106);
            this.MusicNameLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.MusicNameLabel.Name = "MusicNameLabel";
            this.MusicNameLabel.Size = new System.Drawing.Size(136, 21);
            this.MusicNameLabel.TabIndex = 1;
            // 
            // bunifuCustomLabel3
            // 
            this.bunifuCustomLabel3.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.bunifuCustomLabel3.Location = new System.Drawing.Point(14, 106);
            this.bunifuCustomLabel3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.bunifuCustomLabel3.Name = "bunifuCustomLabel3";
            this.bunifuCustomLabel3.Size = new System.Drawing.Size(94, 21);
            this.bunifuCustomLabel3.TabIndex = 1;
            this.bunifuCustomLabel3.Text = "Music Name:";
            // 
            // lblInfo
            // 
            this.lblInfo.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.lblInfo.Location = new System.Drawing.Point(14, 127);
            this.lblInfo.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(56, 21);
            this.lblInfo.TabIndex = 1;
            this.lblInfo.Text = "Singer:";
            // 
            // pctrBoxInfo
            // 
            this.pctrBoxInfo.Location = new System.Drawing.Point(70, 2);
            this.pctrBoxInfo.Margin = new System.Windows.Forms.Padding(2);
            this.pctrBoxInfo.Name = "pctrBoxInfo";
            this.pctrBoxInfo.Size = new System.Drawing.Size(103, 94);
            this.pctrBoxInfo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pctrBoxInfo.TabIndex = 0;
            this.pctrBoxInfo.TabStop = false;
            // 
            // MusicCDDetailForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel1);
            this.Name = "MusicCDDetailForm";
            this.Size = new System.Drawing.Size(260, 250);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericValueQuantity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctrBoxInfo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        public Bunifu.Framework.UI.BunifuFlatButton AddToCartButton;
        public System.Windows.Forms.NumericUpDown numericValueQuantity;
        public Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel2;
        public Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel1;
        public Bunifu.Framework.UI.BunifuCustomLabel MusicPriceLabel;
        public Bunifu.Framework.UI.BunifuCustomLabel MusicTypeLabel;
        public Bunifu.Framework.UI.BunifuCustomLabel MusicSingerLabel;
        public Bunifu.Framework.UI.BunifuCustomLabel MusicNameLabel;
        public Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel3;
        public Bunifu.Framework.UI.BunifuCustomLabel lblInfo;
        public System.Windows.Forms.PictureBox pctrBoxInfo;
    }
}
