﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EntityStore;
using LogicLayerStore;
using DTLStore;

namespace OOP_Project
{
    
    public partial class OrdersForm : Form
    {
        DashBoardForm dashBoardForm;
        List<EntityOrderDB> OrderList;
        private int currentUserID;
        private string currentUserName;
        int sayac = 0;
        public OrdersForm(int currentUserID, string currentUserName,DashBoardForm dashBoardForm)
        {
            this.dashBoardForm = dashBoardForm;
            InitializeComponent();
            this.currentUserID = currentUserID;
            this.currentUserName = currentUserName;
        }
        /// <summary> 
        /// When form opened, necessary values assign
        /// </summary>
        private void OrdersForm_Load(object sender, EventArgs e)
        {
            OrderList = LLOrderDB.LLOrderDBList(currentUserID);
            List<EntityOrderPackage> packageOrderId = LLOrderPackage.LLOrderPackageList(currentUserID); 
            foreach (var packageOrder in packageOrderId)
            {
                List<EntityOrderDB> template = new List<EntityOrderDB>();
                float totalPricePackage = 0;
                foreach (var order in OrderList)
                {
                    if (packageOrder.OrderPackage_orderId==order.Order_PackageId)
                    {
                        totalPricePackage += order.Cart_ProductTotalPrice;
                        template.Add(order);
                    }
                }
                AddProductPackage(template, totalPricePackage);
            }
        }
        /// <summary> 
        /// This function fills the orders panel
        /// </summary>
        /// <param name="package">Product list of order.</param>
        /// <param name="totalPrice">Total price of order.</param>
        public void AddProductPackage(List<EntityOrderDB> package,float totalPrice)
        {
            OrderDetailForm orderDetailForm = new OrderDetailForm();
            List<EntityOrderDB> tmppackage = new List<EntityOrderDB>();
            tmppackage = package;
            orderDetailForm.Dock = DockStyle.Fill;
            orderDetailForm.TotalPriceLabel.Text = totalPrice.ToString()+" TL";
            orderDetailForm.TimeDateLabel.Text = tmppackage[0].DateTime.ToString("MM/dd/yyyy HH:mm:ss");
            // Show the detail of order 
            orderDetailForm.DetailButton.Click += delegate
            {
                sendLog("User clicked to detail of order dated : " + tmppackage[0].DateTime.ToString());
                lstviewCart.Items.Clear();
                foreach (var item in tmppackage)
                {
                    string[] listProductString = new string[4];
                    listProductString[0] = item.ProductId.ToString();
                    listProductString[1] = item.Cart_ProductName.ToString();
                    listProductString[2] = item.ProductQuantity.ToString();
                    listProductString[3] = item.Cart_ProductTotalPrice.ToString();
                    ListViewItem lsitem = new ListViewItem(listProductString);
                    lstviewCart.Items.Add(lsitem);
                }
            };
            // Cancel the order
            orderDetailForm.CancelButton.Click += delegate
            {
                sendLog("User deleted the order dated : " + tmppackage[0].DateTime.ToString());
                foreach (var item in tmppackage)
                {
                    LLOrderDB.LLOrderDelete(item.ProductId, item.CustomerId);
                }
                LLOrderPackage.LLOrderPackageDelete(tmppackage[0].Order_PackageId);
                dashBoardForm.panelSag.Controls.Clear();
                OrdersForm frm = new OrdersForm(this.currentUserID, this.currentUserName, dashBoardForm);
                frm.TopLevel = false;
                frm.AutoScroll = true;
                dashBoardForm.panelSag.Controls.Add(frm);
                frm.Show();
            };
            this.ordersPanel.Controls.Add(orderDetailForm, 0, sayac++);
        }
        /// <summary> 
        /// Send the users log to database
        /// </summary>
        private void sendLog(string customerEvent)
        {
            EntityUserLog userLog = new EntityUserLog();
            userLog.CustomerId = this.currentUserID;
            userLog.CustomerName = this.currentUserName;
            userLog.CustomerEvent = customerEvent;
            userLog.CustomerEventTimeStampt = DateTime.Now;
            LLUserLog.UserLogAdd(userLog);
        }

    }
}
