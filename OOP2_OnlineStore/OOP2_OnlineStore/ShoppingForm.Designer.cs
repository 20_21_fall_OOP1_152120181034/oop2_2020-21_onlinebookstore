﻿
namespace OOP_Project
{
    partial class ShoppingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ShoppingForm));
            this.PanelProducts = new System.Windows.Forms.TableLayoutPanel();
            this.panelCart = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnDeleteProduct = new Bunifu.Framework.UI.BunifuFlatButton();
            this.OrderButton = new Bunifu.Framework.UI.BunifuFlatButton();
            this.lstviewCart = new System.Windows.Forms.ListView();
            this.ProductID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ProductName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Quantity = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Cost = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel2 = new System.Windows.Forms.Panel();
            this.TotalCost = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel2 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.bunifuCustomLabel1 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.btnBooks = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnMagazines = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnMusicCds = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnAllProducts = new Bunifu.Framework.UI.BunifuFlatButton();
            this.panelCart.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // PanelProducts
            // 
            this.PanelProducts.AutoScroll = true;
            this.PanelProducts.ColumnCount = 2;
            this.PanelProducts.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 260F));
            this.PanelProducts.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 270F));
            this.PanelProducts.Location = new System.Drawing.Point(38, 72);
            this.PanelProducts.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.PanelProducts.Name = "PanelProducts";
            this.PanelProducts.RowCount = 2;
            this.PanelProducts.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 250F));
            this.PanelProducts.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 250F));
            this.PanelProducts.Size = new System.Drawing.Size(530, 518);
            this.PanelProducts.TabIndex = 0;
            // 
            // panelCart
            // 
            this.panelCart.BackColor = System.Drawing.SystemColors.Control;
            this.panelCart.Controls.Add(this.panel3);
            this.panelCart.Controls.Add(this.panel2);
            this.panelCart.Controls.Add(this.panel1);
            this.panelCart.Location = new System.Drawing.Point(607, 11);
            this.panelCart.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panelCart.Name = "panelCart";
            this.panelCart.Size = new System.Drawing.Size(315, 542);
            this.panelCart.TabIndex = 1;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Controls.Add(this.lstviewCart);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 45);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(315, 447);
            this.panel3.TabIndex = 2;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnDeleteProduct);
            this.panel4.Controls.Add(this.OrderButton);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.Location = new System.Drawing.Point(0, 397);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(315, 50);
            this.panel4.TabIndex = 1;
            // 
            // btnDeleteProduct
            // 
            this.btnDeleteProduct.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnDeleteProduct.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnDeleteProduct.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDeleteProduct.BorderRadius = 0;
            this.btnDeleteProduct.ButtonText = "Delete Product";
            this.btnDeleteProduct.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDeleteProduct.DisabledColor = System.Drawing.Color.Gray;
            this.btnDeleteProduct.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnDeleteProduct.Iconcolor = System.Drawing.Color.Transparent;
            this.btnDeleteProduct.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnDeleteProduct.Iconimage")));
            this.btnDeleteProduct.Iconimage_right = null;
            this.btnDeleteProduct.Iconimage_right_Selected = null;
            this.btnDeleteProduct.Iconimage_Selected = null;
            this.btnDeleteProduct.IconMarginLeft = 0;
            this.btnDeleteProduct.IconMarginRight = 0;
            this.btnDeleteProduct.IconRightVisible = true;
            this.btnDeleteProduct.IconRightZoom = 0D;
            this.btnDeleteProduct.IconVisible = true;
            this.btnDeleteProduct.IconZoom = 50D;
            this.btnDeleteProduct.IsTab = false;
            this.btnDeleteProduct.Location = new System.Drawing.Point(157, 0);
            this.btnDeleteProduct.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnDeleteProduct.Name = "btnDeleteProduct";
            this.btnDeleteProduct.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnDeleteProduct.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(129)))), ((int)(((byte)(77)))));
            this.btnDeleteProduct.OnHoverTextColor = System.Drawing.Color.White;
            this.btnDeleteProduct.selected = false;
            this.btnDeleteProduct.Size = new System.Drawing.Size(158, 50);
            this.btnDeleteProduct.TabIndex = 1;
            this.btnDeleteProduct.Text = "Delete Product";
            this.btnDeleteProduct.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDeleteProduct.Textcolor = System.Drawing.Color.White;
            this.btnDeleteProduct.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteProduct.Click += new System.EventHandler(this.btnDeleteProduct_Click);
            // 
            // OrderButton
            // 
            this.OrderButton.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.OrderButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(80)))), ((int)(((byte)(173)))));
            this.OrderButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.OrderButton.BorderRadius = 0;
            this.OrderButton.ButtonText = "Make Order";
            this.OrderButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.OrderButton.DisabledColor = System.Drawing.Color.Gray;
            this.OrderButton.Dock = System.Windows.Forms.DockStyle.Left;
            this.OrderButton.Iconcolor = System.Drawing.Color.Transparent;
            this.OrderButton.Iconimage = ((System.Drawing.Image)(resources.GetObject("OrderButton.Iconimage")));
            this.OrderButton.Iconimage_right = null;
            this.OrderButton.Iconimage_right_Selected = null;
            this.OrderButton.Iconimage_Selected = null;
            this.OrderButton.IconMarginLeft = 0;
            this.OrderButton.IconMarginRight = 0;
            this.OrderButton.IconRightVisible = true;
            this.OrderButton.IconRightZoom = 0D;
            this.OrderButton.IconVisible = true;
            this.OrderButton.IconZoom = 50D;
            this.OrderButton.IsTab = false;
            this.OrderButton.Location = new System.Drawing.Point(0, 0);
            this.OrderButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.OrderButton.Name = "OrderButton";
            this.OrderButton.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(80)))), ((int)(((byte)(173)))));
            this.OrderButton.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(129)))), ((int)(((byte)(77)))));
            this.OrderButton.OnHoverTextColor = System.Drawing.Color.White;
            this.OrderButton.selected = false;
            this.OrderButton.Size = new System.Drawing.Size(157, 50);
            this.OrderButton.TabIndex = 0;
            this.OrderButton.Text = "Make Order";
            this.OrderButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.OrderButton.Textcolor = System.Drawing.Color.White;
            this.OrderButton.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OrderButton.Click += new System.EventHandler(this.OrderButton_Click);
            // 
            // lstviewCart
            // 
            this.lstviewCart.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ProductID,
            this.ProductName,
            this.Quantity,
            this.Cost});
            this.lstviewCart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstviewCart.FullRowSelect = true;
            this.lstviewCart.GridLines = true;
            this.lstviewCart.HideSelection = false;
            this.lstviewCart.Location = new System.Drawing.Point(0, 0);
            this.lstviewCart.MultiSelect = false;
            this.lstviewCart.Name = "lstviewCart";
            this.lstviewCart.Size = new System.Drawing.Size(315, 447);
            this.lstviewCart.TabIndex = 0;
            this.lstviewCart.UseCompatibleStateImageBehavior = false;
            this.lstviewCart.View = System.Windows.Forms.View.Details;
            // 
            // ProductID
            // 
            this.ProductID.Text = "ProductID";
            // 
            // ProductName
            // 
            this.ProductName.Text = "Product Name";
            this.ProductName.Width = 135;
            // 
            // Quantity
            // 
            this.Quantity.Text = "Quantity";
            // 
            // Cost
            // 
            this.Cost.Text = "Cost";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Teal;
            this.panel2.Controls.Add(this.TotalCost);
            this.panel2.Controls.Add(this.bunifuCustomLabel2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.ForeColor = System.Drawing.Color.Teal;
            this.panel2.Location = new System.Drawing.Point(0, 492);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(315, 50);
            this.panel2.TabIndex = 1;
            // 
            // TotalCost
            // 
            this.TotalCost.AutoSize = true;
            this.TotalCost.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.TotalCost.ForeColor = System.Drawing.SystemColors.Control;
            this.TotalCost.Location = new System.Drawing.Point(105, 12);
            this.TotalCost.Name = "TotalCost";
            this.TotalCost.Size = new System.Drawing.Size(38, 21);
            this.TotalCost.TabIndex = 2;
            this.TotalCost.Text = "0 TL";
            // 
            // bunifuCustomLabel2
            // 
            this.bunifuCustomLabel2.AutoSize = true;
            this.bunifuCustomLabel2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.bunifuCustomLabel2.ForeColor = System.Drawing.SystemColors.Control;
            this.bunifuCustomLabel2.Location = new System.Drawing.Point(14, 12);
            this.bunifuCustomLabel2.Name = "bunifuCustomLabel2";
            this.bunifuCustomLabel2.Size = new System.Drawing.Size(97, 21);
            this.bunifuCustomLabel2.TabIndex = 2;
            this.bunifuCustomLabel2.Text = "Total Cost: ";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Teal;
            this.panel1.Controls.Add(this.bunifuCustomLabel1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(315, 45);
            this.panel1.TabIndex = 0;
            // 
            // bunifuCustomLabel1
            // 
            this.bunifuCustomLabel1.AutoSize = true;
            this.bunifuCustomLabel1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.bunifuCustomLabel1.ForeColor = System.Drawing.SystemColors.Control;
            this.bunifuCustomLabel1.Location = new System.Drawing.Point(14, 13);
            this.bunifuCustomLabel1.Name = "bunifuCustomLabel1";
            this.bunifuCustomLabel1.Size = new System.Drawing.Size(124, 21);
            this.bunifuCustomLabel1.TabIndex = 0;
            this.bunifuCustomLabel1.Text = "Shopping Cart";
            // 
            // btnBooks
            // 
            this.btnBooks.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnBooks.BackColor = System.Drawing.Color.Teal;
            this.btnBooks.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnBooks.BorderRadius = 0;
            this.btnBooks.ButtonText = "Books";
            this.btnBooks.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBooks.DisabledColor = System.Drawing.Color.Gray;
            this.btnBooks.Iconcolor = System.Drawing.Color.Transparent;
            this.btnBooks.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnBooks.Iconimage")));
            this.btnBooks.Iconimage_right = null;
            this.btnBooks.Iconimage_right_Selected = null;
            this.btnBooks.Iconimage_Selected = null;
            this.btnBooks.IconMarginLeft = 0;
            this.btnBooks.IconMarginRight = 0;
            this.btnBooks.IconRightVisible = true;
            this.btnBooks.IconRightZoom = 0D;
            this.btnBooks.IconVisible = true;
            this.btnBooks.IconZoom = 60D;
            this.btnBooks.IsTab = false;
            this.btnBooks.Location = new System.Drawing.Point(154, 11);
            this.btnBooks.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnBooks.Name = "btnBooks";
            this.btnBooks.Normalcolor = System.Drawing.Color.Teal;
            this.btnBooks.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBooks.OnHoverTextColor = System.Drawing.Color.White;
            this.btnBooks.selected = false;
            this.btnBooks.Size = new System.Drawing.Size(140, 48);
            this.btnBooks.TabIndex = 2;
            this.btnBooks.Text = "Books";
            this.btnBooks.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBooks.Textcolor = System.Drawing.Color.White;
            this.btnBooks.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBooks.Click += new System.EventHandler(this.btnBooks_Click);
            // 
            // btnMagazines
            // 
            this.btnMagazines.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnMagazines.BackColor = System.Drawing.Color.Teal;
            this.btnMagazines.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnMagazines.BorderRadius = 0;
            this.btnMagazines.ButtonText = "Magazines";
            this.btnMagazines.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMagazines.DisabledColor = System.Drawing.Color.Gray;
            this.btnMagazines.Iconcolor = System.Drawing.Color.Transparent;
            this.btnMagazines.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnMagazines.Iconimage")));
            this.btnMagazines.Iconimage_right = null;
            this.btnMagazines.Iconimage_right_Selected = null;
            this.btnMagazines.Iconimage_Selected = null;
            this.btnMagazines.IconMarginLeft = 0;
            this.btnMagazines.IconMarginRight = 0;
            this.btnMagazines.IconRightVisible = true;
            this.btnMagazines.IconRightZoom = 0D;
            this.btnMagazines.IconVisible = true;
            this.btnMagazines.IconZoom = 50D;
            this.btnMagazines.IsTab = false;
            this.btnMagazines.Location = new System.Drawing.Point(301, 11);
            this.btnMagazines.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnMagazines.Name = "btnMagazines";
            this.btnMagazines.Normalcolor = System.Drawing.Color.Teal;
            this.btnMagazines.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnMagazines.OnHoverTextColor = System.Drawing.Color.White;
            this.btnMagazines.selected = false;
            this.btnMagazines.Size = new System.Drawing.Size(140, 48);
            this.btnMagazines.TabIndex = 3;
            this.btnMagazines.Text = "Magazines";
            this.btnMagazines.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMagazines.Textcolor = System.Drawing.Color.White;
            this.btnMagazines.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMagazines.Click += new System.EventHandler(this.btnMagazines_Click);
            // 
            // btnMusicCds
            // 
            this.btnMusicCds.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnMusicCds.BackColor = System.Drawing.Color.Teal;
            this.btnMusicCds.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnMusicCds.BorderRadius = 0;
            this.btnMusicCds.ButtonText = "Music CD";
            this.btnMusicCds.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMusicCds.DisabledColor = System.Drawing.Color.Gray;
            this.btnMusicCds.Iconcolor = System.Drawing.Color.Transparent;
            this.btnMusicCds.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnMusicCds.Iconimage")));
            this.btnMusicCds.Iconimage_right = null;
            this.btnMusicCds.Iconimage_right_Selected = null;
            this.btnMusicCds.Iconimage_Selected = null;
            this.btnMusicCds.IconMarginLeft = 0;
            this.btnMusicCds.IconMarginRight = 0;
            this.btnMusicCds.IconRightVisible = true;
            this.btnMusicCds.IconRightZoom = 0D;
            this.btnMusicCds.IconVisible = true;
            this.btnMusicCds.IconZoom = 50D;
            this.btnMusicCds.IsTab = false;
            this.btnMusicCds.Location = new System.Drawing.Point(448, 11);
            this.btnMusicCds.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnMusicCds.Name = "btnMusicCds";
            this.btnMusicCds.Normalcolor = System.Drawing.Color.Teal;
            this.btnMusicCds.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnMusicCds.OnHoverTextColor = System.Drawing.Color.White;
            this.btnMusicCds.selected = false;
            this.btnMusicCds.Size = new System.Drawing.Size(140, 48);
            this.btnMusicCds.TabIndex = 4;
            this.btnMusicCds.Text = "Music CD";
            this.btnMusicCds.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMusicCds.Textcolor = System.Drawing.Color.White;
            this.btnMusicCds.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMusicCds.Click += new System.EventHandler(this.btnMusicCds_Click);
            // 
            // btnAllProducts
            // 
            this.btnAllProducts.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnAllProducts.BackColor = System.Drawing.Color.Teal;
            this.btnAllProducts.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAllProducts.BorderRadius = 0;
            this.btnAllProducts.ButtonText = "All Products";
            this.btnAllProducts.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAllProducts.DisabledColor = System.Drawing.Color.Gray;
            this.btnAllProducts.Iconcolor = System.Drawing.Color.Transparent;
            this.btnAllProducts.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnAllProducts.Iconimage")));
            this.btnAllProducts.Iconimage_right = null;
            this.btnAllProducts.Iconimage_right_Selected = null;
            this.btnAllProducts.Iconimage_Selected = null;
            this.btnAllProducts.IconMarginLeft = 0;
            this.btnAllProducts.IconMarginRight = 0;
            this.btnAllProducts.IconRightVisible = true;
            this.btnAllProducts.IconRightZoom = 0D;
            this.btnAllProducts.IconVisible = true;
            this.btnAllProducts.IconZoom = 50D;
            this.btnAllProducts.IsTab = false;
            this.btnAllProducts.Location = new System.Drawing.Point(7, 11);
            this.btnAllProducts.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnAllProducts.Name = "btnAllProducts";
            this.btnAllProducts.Normalcolor = System.Drawing.Color.Teal;
            this.btnAllProducts.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnAllProducts.OnHoverTextColor = System.Drawing.Color.White;
            this.btnAllProducts.selected = false;
            this.btnAllProducts.Size = new System.Drawing.Size(140, 48);
            this.btnAllProducts.TabIndex = 5;
            this.btnAllProducts.Text = "All Products";
            this.btnAllProducts.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAllProducts.Textcolor = System.Drawing.Color.White;
            this.btnAllProducts.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAllProducts.Click += new System.EventHandler(this.btnAllProducts_Click);
            // 
            // ShoppingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(933, 564);
            this.Controls.Add(this.btnAllProducts);
            this.Controls.Add(this.btnMusicCds);
            this.Controls.Add(this.btnMagazines);
            this.Controls.Add(this.btnBooks);
            this.Controls.Add(this.panelCart);
            this.Controls.Add(this.PanelProducts);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "ShoppingForm";
            this.Text = "FormWindowD";
            this.Load += new System.EventHandler(this.FormWindowD_Load);
            this.panelCart.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel PanelProducts;
        private System.Windows.Forms.Panel panelCart;
        private System.Windows.Forms.Panel panel2;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel2;
        private System.Windows.Forms.Panel panel1;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ColumnHeader ProductName;
        private System.Windows.Forms.ColumnHeader Cost;
        private Bunifu.Framework.UI.BunifuCustomLabel TotalCost;
        private System.Windows.Forms.Panel panel4;
        private Bunifu.Framework.UI.BunifuFlatButton btnDeleteProduct;
        private System.Windows.Forms.ColumnHeader Quantity;
        private System.Windows.Forms.ColumnHeader ProductID;
        private Bunifu.Framework.UI.BunifuFlatButton btnBooks;
        private Bunifu.Framework.UI.BunifuFlatButton btnMagazines;
        private Bunifu.Framework.UI.BunifuFlatButton btnMusicCds;
        private Bunifu.Framework.UI.BunifuFlatButton btnAllProducts;
        public Bunifu.Framework.UI.BunifuFlatButton OrderButton;
        public System.Windows.Forms.ListView lstviewCart;
    }
}