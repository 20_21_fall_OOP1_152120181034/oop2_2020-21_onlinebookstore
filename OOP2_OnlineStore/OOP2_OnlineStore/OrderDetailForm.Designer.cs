﻿
namespace OOP_Project
{
    partial class OrderDetailForm
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OrderDetailForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.DetailButton = new Bunifu.Framework.UI.BunifuThinButton2();
            this.panel2 = new System.Windows.Forms.Panel();
            this.TimeDateLabel = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.TotalPriceLabel = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.CancelButton = new Bunifu.Framework.UI.BunifuImageButton();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CancelButton)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.DetailButton);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(119, 55);
            this.panel1.TabIndex = 0;
            // 
            // DetailButton
            // 
            this.DetailButton.ActiveBorderThickness = 1;
            this.DetailButton.ActiveCornerRadius = 20;
            this.DetailButton.ActiveFillColor = System.Drawing.Color.SeaGreen;
            this.DetailButton.ActiveForecolor = System.Drawing.Color.White;
            this.DetailButton.ActiveLineColor = System.Drawing.Color.SeaGreen;
            this.DetailButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.DetailButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("DetailButton.BackgroundImage")));
            this.DetailButton.ButtonText = "DETAIL";
            this.DetailButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.DetailButton.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.DetailButton.ForeColor = System.Drawing.SystemColors.Control;
            this.DetailButton.IdleBorderThickness = 1;
            this.DetailButton.IdleCornerRadius = 20;
            this.DetailButton.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.DetailButton.IdleForecolor = System.Drawing.Color.SeaGreen;
            this.DetailButton.IdleLineColor = System.Drawing.Color.SeaGreen;
            this.DetailButton.Location = new System.Drawing.Point(5, 10);
            this.DetailButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.DetailButton.Name = "DetailButton";
            this.DetailButton.Size = new System.Drawing.Size(107, 34);
            this.DetailButton.TabIndex = 0;
            this.DetailButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TimeDateLabel);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(119, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(220, 55);
            this.panel2.TabIndex = 1;
            // 
            // TimeDateLabel
            // 
            this.TimeDateLabel.AutoSize = true;
            this.TimeDateLabel.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.TimeDateLabel.ForeColor = System.Drawing.SystemColors.Control;
            this.TimeDateLabel.Location = new System.Drawing.Point(33, 17);
            this.TimeDateLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.TimeDateLabel.Name = "TimeDateLabel";
            this.TimeDateLabel.Size = new System.Drawing.Size(138, 21);
            this.TimeDateLabel.TabIndex = 0;
            this.TimeDateLabel.Text = "02.06.2021 22:30";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.TotalPriceLabel);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(339, 0);
            this.panel3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(220, 55);
            this.panel3.TabIndex = 2;
            // 
            // TotalPriceLabel
            // 
            this.TotalPriceLabel.AutoSize = true;
            this.TotalPriceLabel.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.TotalPriceLabel.ForeColor = System.Drawing.SystemColors.Control;
            this.TotalPriceLabel.Location = new System.Drawing.Point(76, 17);
            this.TotalPriceLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.TotalPriceLabel.Name = "TotalPriceLabel";
            this.TotalPriceLabel.Size = new System.Drawing.Size(58, 21);
            this.TotalPriceLabel.TabIndex = 0;
            this.TotalPriceLabel.Text = "130 TL";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.CancelButton);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel4.Location = new System.Drawing.Point(559, 0);
            this.panel4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(119, 55);
            this.panel4.TabIndex = 3;
            // 
            // CancelButton
            // 
            this.CancelButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.CancelButton.Image = ((System.Drawing.Image)(resources.GetObject("CancelButton.Image")));
            this.CancelButton.ImageActive = null;
            this.CancelButton.Location = new System.Drawing.Point(36, 4);
            this.CancelButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(51, 47);
            this.CancelButton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.CancelButton.TabIndex = 0;
            this.CancelButton.TabStop = false;
            this.CancelButton.Zoom = 10;
            // 
            // OrderDetailForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "OrderDetailForm";
            this.Size = new System.Drawing.Size(679, 55);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CancelButton)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        public Bunifu.Framework.UI.BunifuThinButton2 DetailButton;
        public System.Windows.Forms.Label TimeDateLabel;
        public System.Windows.Forms.Label TotalPriceLabel;
        private System.Windows.Forms.Panel panel4;
        public Bunifu.Framework.UI.BunifuImageButton CancelButton;
    }
}
