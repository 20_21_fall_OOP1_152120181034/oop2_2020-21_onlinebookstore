﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EntityStore;
using DTLStore;
using LogicLayerStore;
namespace OOP_Project
{
    public partial class ShoppingForm : Form
    {
        List<EntityBook> books = LLBook.LLBookList();
        List<EntityMagazine> magazines = LLMagazine.LLMagazineList();
        List<EntityMusicCd> musicCd = LLMusicCd.LLMusicCdList();
        List<EntityShoppingCart> shoppingCarts;
        List<EntityOrderPackage> orderPackages;
        private int currentUserID;
        private string currentUserName;
        private Bunifu.Framework.UI.BunifuFlatButton currentButton;

       
        public ShoppingForm(int currentUserID, string currentUserName)
        {
            InitializeComponent();
            this.currentUserID = currentUserID;
            this.currentUserName = currentUserName;
        }
        /// <summary> 
        /// When form opened, necessary values assign
        /// </summary>
        private void FormWindowD_Load(object sender, EventArgs e)
        {
            shoppingCarts = LLShoppingCart.LLShoppingCartList(currentUserID);
            foreach (var item in shoppingCarts)
            {
                FillShoppingCart(item.ProductId,item.Cart_ProductName,item.ProductQuantity,item.Cart_ProductTotalPrice);
            }
            AddBooks();
            AddMagazines();
            AddMusicCds();
            currentButton = btnAllProducts;
        }

        /// <summary> 
        /// Fill the shopping cart list
        /// </summary>
        /// <param name="productId">Id of product.</param>
        /// <param name="productName">Name of product.</param>
        /// <param name="quantity">Count of product.</param>
        /// <param name="totalPrice">TotalPrice of product.</param>
        void FillShoppingCart(int productId,string productName,int quantity,float totalPrice)
        {
            string[] arr=new string[4];
            arr[0] = productId.ToString();
            arr[1] = productName;
            arr[2] = quantity.ToString();
            arr[3] = totalPrice.ToString();
            ListViewItem item = new ListViewItem(arr);
            lstviewCart.Items.Add(item);
            TotalCost.Text = (Convert.ToDouble(TotalCost.Text.Remove(TotalCost.Text.Length - 3, 3)) + totalPrice).ToString("0.00") + " TL";
        }
        /// <summary> 
        /// Show the books in the panel
        /// </summary>
        void AddBooks()
        {
            int col = 0;
            int row = 0;
            foreach (var item in books)
            {
                BooksDetailForm pro = new BooksDetailForm();
                pro.pctrBoxInfo.Image = Image.FromFile(System.IO.Path.Combine(Environment.CurrentDirectory, @"ProductImages\", item.ProductImageName));
                pro.BookNameLabel.Text = item.ProductName;
                pro.BookAuthorLabel.Text = item.ProductAuthor;
                pro.BookPublisherLabel.Text = item.ProductPublisher;
                pro.BookISBNLabel.Text = item.ProductISBNNumber;
                pro.BookPagesLabel.Text = item.ProductPage.ToString();
                pro.BookPriceLabel.Text = item.ProductPrice.ToString() + " TL";
                pro.Dock = DockStyle.Fill;
                bool flag = false;
                pro.AddToCartButton.Click += delegate
                {
                    shoppingCarts = LLShoppingCart.LLShoppingCartList(currentUserID);
                    foreach (var cartItem in shoppingCarts)
                    {
                        if (cartItem.ProductId==item.ProductId)
                        {
                            flag = true;
                            if (MessageBox.Show("This product already been added to cart. Are you sure you want to add again?","Warning",MessageBoxButtons.YesNo,MessageBoxIcon.Warning)==DialogResult.Yes)
                            {
                                sendLog("User added " + item.ProductName + " into cart");
                                LLShoppingCart.BookUpdate(item.ProductId, currentUserID, cartItem.ProductQuantity + Convert.ToInt32(pro.numericValueQuantity.Value),(cartItem.ProductQuantity + Convert.ToInt32(pro.numericValueQuantity.Value)) * item.ProductPrice);
                                foreach (ListViewItem changeItem  in lstviewCart.Items)
                                {
                                    if (changeItem.SubItems[0].Text==item.ProductId.ToString())
                                    {
                                        changeItem.SubItems[2].Text = (cartItem.ProductQuantity + Convert.ToInt32(pro.numericValueQuantity.Value)).ToString();
                                        changeItem.SubItems[3].Text = ((cartItem.ProductQuantity + Convert.ToInt32(pro.numericValueQuantity.Value)) * item.ProductPrice).ToString();
                                        TotalCost.Text = (Convert.ToDouble(TotalCost.Text.Remove(TotalCost.Text.Length - 3, 3)) + Convert.ToInt32(pro.numericValueQuantity.Value)*item.ProductPrice).ToString("0.00") + " TL";
                                        break;
                                    }
                                }
                            }
                            break; 
                        }
                        shoppingCarts = LLShoppingCart.LLShoppingCartList(currentUserID);
                    }
                    if (!flag)
                    {
                        sendLog("User added " + item.ProductName + " into cart");
                        LLShoppingCart.LLBookAdd(item.ProductId, currentUserID, int.Parse(pro.numericValueQuantity.Value.ToString()), float.Parse((item.ProductPrice * int.Parse(pro.numericValueQuantity.Value.ToString())).ToString()), item.ProductName);
                        FillShoppingCart(item.ProductId,item.ProductName,int.Parse(pro.numericValueQuantity.Value.ToString()), item.ProductPrice * int.Parse(pro.numericValueQuantity.Value.ToString()));
                        shoppingCarts = LLShoppingCart.LLShoppingCartList(currentUserID);
                    }
                };
                this.PanelProducts.Controls.Add(pro, col++, row);
                if (col == 2)
                {
                    row++;
                    col = 0;
                }
            }
        }
        /// <summary> 
        /// Show the magazines in the panel
        /// </summary>
        void AddMagazines()
        {
            int col = 0;
            int row = 0;
            foreach (var item in magazines)
            {
                MagazinesDetailForm pro = new MagazinesDetailForm();
                pro.pctrBoxInfo.Image = Image.FromFile(System.IO.Path.Combine(Environment.CurrentDirectory, @"ProductImages\", item.ProductImageName));
                pro.MagazineNameLabel.Text = item.ProductName;
                
                pro.MagazineIssueLabel.Text = item.ProductIssue;
                pro.MagazineTypeLabel.Text = item.ProductType;
                pro.MagazinePriceLabel.Text = item.ProductPrice.ToString() + " TL";
                pro.Dock = DockStyle.Fill;

                bool flag = false;
                pro.AddToCartButton.Click += delegate
                {
                    shoppingCarts = LLShoppingCart.LLShoppingCartList(currentUserID);
                    foreach (var cartItem in shoppingCarts)
                    {
                        if (cartItem.ProductId == item.ProductId)
                        {
                            flag = true;
                            if (MessageBox.Show("This product already been added to cart. Are you sure you want to add again?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                            {
                                sendLog("User added " + item.ProductName + " into cart");
                                LLShoppingCart.BookUpdate(item.ProductId, currentUserID, cartItem.ProductQuantity + Convert.ToInt32(pro.numericValueQuantity.Value), (cartItem.ProductQuantity + Convert.ToInt32(pro.numericValueQuantity.Value)) * item.ProductPrice);
                                foreach (ListViewItem changeItem in lstviewCart.Items)
                                {
                                    if (changeItem.SubItems[0].Text == item.ProductId.ToString())
                                    {
                                        changeItem.SubItems[2].Text = (cartItem.ProductQuantity + Convert.ToInt32(pro.numericValueQuantity.Value)).ToString();
                                        changeItem.SubItems[3].Text = ((cartItem.ProductQuantity + Convert.ToInt32(pro.numericValueQuantity.Value)) * item.ProductPrice).ToString();
                                        TotalCost.Text = (Convert.ToDouble(TotalCost.Text.Remove(TotalCost.Text.Length - 3, 3)) + Convert.ToInt32(pro.numericValueQuantity.Value) * item.ProductPrice).ToString("0.00") + " TL";
                                        break;
                                    }
                                }
                            }
                            break;
                        }
                        shoppingCarts = LLShoppingCart.LLShoppingCartList(currentUserID);
                    }
                    if (!flag)
                    {
                        sendLog("User added " + item.ProductName + " into cart");
                        LLShoppingCart.LLBookAdd(item.ProductId, currentUserID, int.Parse(pro.numericValueQuantity.Value.ToString()), float.Parse((item.ProductPrice * int.Parse(pro.numericValueQuantity.Value.ToString())).ToString()), item.ProductName);
                        FillShoppingCart(item.ProductId, item.ProductName, int.Parse(pro.numericValueQuantity.Value.ToString()), item.ProductPrice * int.Parse(pro.numericValueQuantity.Value.ToString()));
                        shoppingCarts = LLShoppingCart.LLShoppingCartList(currentUserID);
                    }
                };
                this.PanelProducts.Controls.Add(pro, col++, row);
                if (col == 2)
                {
                    row++;
                    col = 0;
                }
            }
        }
        /// <summary> 
        /// Show the musicCds in the panel
        /// </summary>
        void AddMusicCds()
        {
            int col = 0;
            int row = 0;
            foreach (var item in musicCd)
            {
                MusicCDDetailForm pro = new MusicCDDetailForm();
                pro.pctrBoxInfo.Image = Image.FromFile(System.IO.Path.Combine(Environment.CurrentDirectory, @"ProductImages\", item.ProductImageName));
                pro.MusicNameLabel.Text = item.ProductName;
                pro.MusicSingerLabel.Text = item.ProductSinger;
                pro.MusicTypeLabel.Text = item.ProductType;
                pro.MusicPriceLabel.Text = item.ProductPrice.ToString() + " TL";
                pro.Dock = DockStyle.Fill;
                bool flag = false;
                pro.AddToCartButton.Click += delegate
                {
                    shoppingCarts = LLShoppingCart.LLShoppingCartList(currentUserID);
                    foreach (var cartItem in shoppingCarts)
                    {

                        if (cartItem.ProductId == item.ProductId)
                        {
                            flag = true;
                            if (MessageBox.Show("This product already been added to cart. Are you sure you want to add again?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                            {
                                sendLog("User added " + item.ProductName + " into cart");
                                LLShoppingCart.BookUpdate(item.ProductId, currentUserID, cartItem.ProductQuantity + Convert.ToInt32(pro.numericValueQuantity.Value), (cartItem.ProductQuantity + Convert.ToInt32(pro.numericValueQuantity.Value)) * item.ProductPrice);
                                foreach (ListViewItem changeItem in lstviewCart.Items)
                                {
                                    if (changeItem.SubItems[0].Text == item.ProductId.ToString())
                                    {
                                        changeItem.SubItems[2].Text = (cartItem.ProductQuantity + Convert.ToInt32(pro.numericValueQuantity.Value)).ToString();
                                        changeItem.SubItems[3].Text = ((cartItem.ProductQuantity + Convert.ToInt32(pro.numericValueQuantity.Value)) * item.ProductPrice).ToString();
                                        TotalCost.Text = (Convert.ToDouble(TotalCost.Text.Remove(TotalCost.Text.Length - 3, 3)) + Convert.ToInt32(pro.numericValueQuantity.Value) * item.ProductPrice).ToString("0.00") + " TL";
                                        break;
                                    }
                                }
                            }
                            break;
                        }
                        shoppingCarts = LLShoppingCart.LLShoppingCartList(currentUserID);
                    }
                    if (!flag)
                    {
                        sendLog("User added " + item.ProductName + " into cart");
                        LLShoppingCart.LLBookAdd(item.ProductId, currentUserID, int.Parse(pro.numericValueQuantity.Value.ToString()), float.Parse((item.ProductPrice * int.Parse(pro.numericValueQuantity.Value.ToString())).ToString()), item.ProductName);
                        FillShoppingCart(item.ProductId, item.ProductName, int.Parse(pro.numericValueQuantity.Value.ToString()), item.ProductPrice * int.Parse(pro.numericValueQuantity.Value.ToString()));
                        shoppingCarts = LLShoppingCart.LLShoppingCartList(currentUserID);
                    }
                };
                this.PanelProducts.Controls.Add(pro, col++, row);
                if (col == 2)
                {
                    row++;
                    col = 0;
                }
            }
        }
        /// <summary> 
        /// Delete the product from DataBase and Cartlistview
        /// </summary>
        private void btnDeleteProduct_Click(object sender, EventArgs e)
        {
            if (lstviewCart.SelectedItems.Count>0)
            {
                sendLog("User removed " + lstviewCart.SelectedItems[0].SubItems[1].Text + " from cart");
                TotalCost.Text = (Convert.ToDouble(TotalCost.Text.Remove(TotalCost.Text.Length - 3, 3)) - float.Parse(lstviewCart.SelectedItems[0].SubItems[3].Text)).ToString("0.00") + " TL";
                LLShoppingCart.LLBookDelete(int.Parse(lstviewCart.SelectedItems[0].SubItems[0].Text), currentUserID);
                lstviewCart.Items.Remove(lstviewCart.SelectedItems[0]);
                shoppingCarts = LLShoppingCart.LLShoppingCartList(currentUserID);
            }
        }
        /// <summary> 
        /// Show all products from DataBase
        /// </summary>
        private void btnAllProducts_Click(object sender, EventArgs e)
        {
            if (btnAllProducts.Normalcolor == Color.Teal)
            {
                sendLog("User listed all products");
                activateButton(btnAllProducts);
                this.PanelProducts.Controls.Clear();
                AddBooks();
                AddMagazines();
                AddMusicCds();
            }
        }
        /// <summary> 
        /// Show just books from DataBase
        /// </summary>
        private void btnBooks_Click(object sender, EventArgs e)
        {
            if (btnBooks.Normalcolor == Color.Teal)
            {
                sendLog("User listed only books");
                activateButton(btnBooks);
                this.PanelProducts.Controls.Clear();
                AddBooks();
            }
        }
        /// <summary> 
        /// Show just magazines from DataBase
        /// </summary>
        private void btnMagazines_Click(object sender, EventArgs e)
        {
            if (btnMagazines.Normalcolor == Color.Teal)
            {
                sendLog("User listed only Magazines");
                activateButton(btnMagazines);
                this.PanelProducts.Controls.Clear();
                AddMagazines();
            }
        }
        /// <summary> 
        /// Show just musicCds from DataBase
        /// </summary>
        private void btnMusicCds_Click(object sender, EventArgs e)
        {
            if (btnMusicCds.Normalcolor == Color.Teal)
            {
                sendLog("User listed only MusicCDs");
                activateButton(btnMusicCds);
                this.PanelProducts.Controls.Clear();
                AddMusicCds();
            }
        }
        /// <summary> 
        /// Add the whole products from cartDataBase to orderDatabase
        /// </summary>
        private void OrderButton_Click(object sender, EventArgs e)
        {
            if (lstviewCart.Items.Count!=0&&MessageBox.Show("Are you sure place an order?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                LLOrderPackage.LLOrderPackageAdd(currentUserID);
                orderPackages = LLOrderPackage.LLOrderPackageList(currentUserID);
                sendLog("User created new order");
                shoppingCarts = LLShoppingCart.LLShoppingCartList(currentUserID);
                foreach (var item in shoppingCarts)
                {
                    LLOrderDB.LLOrderAdd(item.ProductId, currentUserID, item.ProductQuantity, item.Cart_ProductTotalPrice, item.Cart_ProductName, DateTime.UtcNow,orderPackages[orderPackages.Count-1].OrderPackage_orderId);
                    LLShoppingCart.LLBookDelete(item.ProductId, currentUserID);
                }
                shoppingCarts = LLShoppingCart.LLShoppingCartList(currentUserID);
                TotalCost.Text ="0 TL";
            }
            
        }
        /// <summary> 
        /// When click the button ones, deactive this button
        /// </summary>
        private void activateButton(Bunifu.Framework.UI.BunifuFlatButton btn)
        {
            deactivateButton();
            btn.Normalcolor= Color.FromArgb(255, 0, 64, 64);
            currentButton = btn;
        }
        /// <summary> 
        /// When click the otter button, active this button
        /// </summary>
        private void deactivateButton()
        {
            currentButton.Normalcolor = Color.Teal;
        }
        /// <summary> 
        /// Send the users log to database
        /// </summary>
        private void sendLog(string customerEvent)
        {
            EntityUserLog userLog = new EntityUserLog();
            userLog.CustomerId = this.currentUserID;
            userLog.CustomerName = this.currentUserName;
            userLog.CustomerEvent = customerEvent;
            userLog.CustomerEventTimeStampt = DateTime.Now;
            LLUserLog.UserLogAdd(userLog);
        }
    }
}
