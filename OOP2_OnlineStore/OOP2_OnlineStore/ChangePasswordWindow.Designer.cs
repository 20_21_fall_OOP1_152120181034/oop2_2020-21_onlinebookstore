﻿
namespace OOP_Project
{
    partial class ChangePasswordWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChangePasswordWindow));
            this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.lblSecondPasswordError = new System.Windows.Forms.Label();
            this.lblFirstPasswordError = new System.Windows.Forms.Label();
            this.txtbxPasswordTwo = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtbxPasswordOne = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label4 = new System.Windows.Forms.Label();
            this.lblOldPasswordError = new System.Windows.Forms.Label();
            this.txtbxOldPassword = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label2 = new System.Windows.Forms.Label();
            this.ConfirmButton = new Bunifu.Framework.UI.BunifuImageButton();
            this.CancelButton = new Bunifu.Framework.UI.BunifuImageButton();
            ((System.ComponentModel.ISupportInitialize)(this.ConfirmButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CancelButton)).BeginInit();
            this.SuspendLayout();
            // 
            // bunifuElipse1
            // 
            this.bunifuElipse1.ElipseRadius = 25;
            this.bunifuElipse1.TargetControl = this;
            // 
            // lblSecondPasswordError
            // 
            this.lblSecondPasswordError.AutoSize = true;
            this.lblSecondPasswordError.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblSecondPasswordError.ForeColor = System.Drawing.Color.Red;
            this.lblSecondPasswordError.Location = new System.Drawing.Point(167, 166);
            this.lblSecondPasswordError.Name = "lblSecondPasswordError";
            this.lblSecondPasswordError.Size = new System.Drawing.Size(36, 17);
            this.lblSecondPasswordError.TabIndex = 14;
            this.lblSecondPasswordError.Text = "Error";
            this.lblSecondPasswordError.Visible = false;
            // 
            // lblFirstPasswordError
            // 
            this.lblFirstPasswordError.AutoSize = true;
            this.lblFirstPasswordError.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblFirstPasswordError.ForeColor = System.Drawing.Color.Red;
            this.lblFirstPasswordError.Location = new System.Drawing.Point(101, 102);
            this.lblFirstPasswordError.Name = "lblFirstPasswordError";
            this.lblFirstPasswordError.Size = new System.Drawing.Size(36, 17);
            this.lblFirstPasswordError.TabIndex = 15;
            this.lblFirstPasswordError.Text = "Error";
            this.lblFirstPasswordError.Visible = false;
            // 
            // txtbxPasswordTwo
            // 
            this.txtbxPasswordTwo.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txtbxPasswordTwo.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtbxPasswordTwo.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtbxPasswordTwo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtbxPasswordTwo.HintForeColor = System.Drawing.Color.Empty;
            this.txtbxPasswordTwo.HintText = "Password";
            this.txtbxPasswordTwo.isPassword = true;
            this.txtbxPasswordTwo.LineFocusedColor = System.Drawing.Color.Teal;
            this.txtbxPasswordTwo.LineIdleColor = System.Drawing.Color.Teal;
            this.txtbxPasswordTwo.LineMouseHoverColor = System.Drawing.Color.Teal;
            this.txtbxPasswordTwo.LineThickness = 3;
            this.txtbxPasswordTwo.Location = new System.Drawing.Point(22, 181);
            this.txtbxPasswordTwo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtbxPasswordTwo.Name = "txtbxPasswordTwo";
            this.txtbxPasswordTwo.Size = new System.Drawing.Size(297, 31);
            this.txtbxPasswordTwo.TabIndex = 13;
            this.txtbxPasswordTwo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtbxPasswordTwo.OnValueChanged += new System.EventHandler(this.txtbxPasswordTwo_OnValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Teal;
            this.label5.Location = new System.Drawing.Point(17, 164);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(156, 21);
            this.label5.TabIndex = 10;
            this.label5.Text = "Re-Enter Password:";
            // 
            // txtbxPasswordOne
            // 
            this.txtbxPasswordOne.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txtbxPasswordOne.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtbxPasswordOne.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtbxPasswordOne.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtbxPasswordOne.HintForeColor = System.Drawing.Color.Empty;
            this.txtbxPasswordOne.HintText = "Password";
            this.txtbxPasswordOne.isPassword = true;
            this.txtbxPasswordOne.LineFocusedColor = System.Drawing.Color.Teal;
            this.txtbxPasswordOne.LineIdleColor = System.Drawing.Color.Teal;
            this.txtbxPasswordOne.LineMouseHoverColor = System.Drawing.Color.Teal;
            this.txtbxPasswordOne.LineThickness = 3;
            this.txtbxPasswordOne.Location = new System.Drawing.Point(22, 117);
            this.txtbxPasswordOne.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtbxPasswordOne.Name = "txtbxPasswordOne";
            this.txtbxPasswordOne.Size = new System.Drawing.Size(297, 31);
            this.txtbxPasswordOne.TabIndex = 12;
            this.txtbxPasswordOne.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtbxPasswordOne.OnValueChanged += new System.EventHandler(this.txtbxPasswordOne_OnValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Teal;
            this.label4.Location = new System.Drawing.Point(17, 100);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 21);
            this.label4.TabIndex = 11;
            this.label4.Text = "Password:";
            // 
            // lblOldPasswordError
            // 
            this.lblOldPasswordError.AutoSize = true;
            this.lblOldPasswordError.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblOldPasswordError.ForeColor = System.Drawing.Color.Red;
            this.lblOldPasswordError.Location = new System.Drawing.Point(132, 35);
            this.lblOldPasswordError.Name = "lblOldPasswordError";
            this.lblOldPasswordError.Size = new System.Drawing.Size(36, 17);
            this.lblOldPasswordError.TabIndex = 18;
            this.lblOldPasswordError.Text = "Error";
            this.lblOldPasswordError.Visible = false;
            // 
            // txtbxOldPassword
            // 
            this.txtbxOldPassword.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txtbxOldPassword.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtbxOldPassword.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtbxOldPassword.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtbxOldPassword.HintForeColor = System.Drawing.Color.Empty;
            this.txtbxOldPassword.HintText = "Password";
            this.txtbxOldPassword.isPassword = true;
            this.txtbxOldPassword.LineFocusedColor = System.Drawing.Color.Teal;
            this.txtbxOldPassword.LineIdleColor = System.Drawing.Color.Teal;
            this.txtbxOldPassword.LineMouseHoverColor = System.Drawing.Color.Teal;
            this.txtbxOldPassword.LineThickness = 3;
            this.txtbxOldPassword.Location = new System.Drawing.Point(22, 50);
            this.txtbxOldPassword.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtbxOldPassword.Name = "txtbxOldPassword";
            this.txtbxOldPassword.Size = new System.Drawing.Size(297, 31);
            this.txtbxOldPassword.TabIndex = 17;
            this.txtbxOldPassword.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtbxOldPassword.OnValueChanged += new System.EventHandler(this.txtbxOldPassword_OnValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Teal;
            this.label2.Location = new System.Drawing.Point(17, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(118, 21);
            this.label2.TabIndex = 16;
            this.label2.Text = "Old Password:";
            // 
            // ConfirmButton
            // 
            this.ConfirmButton.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.ConfirmButton.Image = ((System.Drawing.Image)(resources.GetObject("ConfirmButton.Image")));
            this.ConfirmButton.ImageActive = null;
            this.ConfirmButton.Location = new System.Drawing.Point(89, 238);
            this.ConfirmButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ConfirmButton.Name = "ConfirmButton";
            this.ConfirmButton.Size = new System.Drawing.Size(45, 49);
            this.ConfirmButton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ConfirmButton.TabIndex = 19;
            this.ConfirmButton.TabStop = false;
            this.ConfirmButton.Zoom = 10;
            this.ConfirmButton.Click += new System.EventHandler(this.ConfirmButton_Click);
            // 
            // CancelButton
            // 
            this.CancelButton.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.CancelButton.Image = ((System.Drawing.Image)(resources.GetObject("CancelButton.Image")));
            this.CancelButton.ImageActive = null;
            this.CancelButton.Location = new System.Drawing.Point(195, 238);
            this.CancelButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(45, 49);
            this.CancelButton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.CancelButton.TabIndex = 20;
            this.CancelButton.TabStop = false;
            this.CancelButton.Zoom = 10;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // ChangePasswordWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.ClientSize = new System.Drawing.Size(373, 305);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.ConfirmButton);
            this.Controls.Add(this.lblOldPasswordError);
            this.Controls.Add(this.txtbxOldPassword);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblSecondPasswordError);
            this.Controls.Add(this.lblFirstPasswordError);
            this.Controls.Add(this.txtbxPasswordTwo);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtbxPasswordOne);
            this.Controls.Add(this.label4);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "ChangePasswordWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ChangePasswordWindow";
            ((System.ComponentModel.ISupportInitialize)(this.ConfirmButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CancelButton)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
        private Bunifu.Framework.UI.BunifuImageButton CancelButton;
        private System.Windows.Forms.Label lblOldPasswordError;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtbxOldPassword;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblSecondPasswordError;
        private System.Windows.Forms.Label lblFirstPasswordError;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtbxPasswordTwo;
        private System.Windows.Forms.Label label5;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtbxPasswordOne;
        private System.Windows.Forms.Label label4;
        public Bunifu.Framework.UI.BunifuImageButton ConfirmButton;
    }
}