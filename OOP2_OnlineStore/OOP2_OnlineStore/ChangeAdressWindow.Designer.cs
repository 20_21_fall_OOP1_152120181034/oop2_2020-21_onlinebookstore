﻿
namespace OOP_Project
{
    partial class ChangeAdressWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChangeAdressWindow));
            this.bunifuElipse2 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.lblAdressError = new System.Windows.Forms.Label();
            this.txtbxAdress = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label12 = new System.Windows.Forms.Label();
            this.CancelButton = new Bunifu.Framework.UI.BunifuImageButton();
            this.ConfirmButton = new Bunifu.Framework.UI.BunifuImageButton();
            ((System.ComponentModel.ISupportInitialize)(this.CancelButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConfirmButton)).BeginInit();
            this.SuspendLayout();
            // 
            // bunifuElipse2
            // 
            this.bunifuElipse2.ElipseRadius = 25;
            this.bunifuElipse2.TargetControl = this;
            // 
            // lblAdressError
            // 
            this.lblAdressError.AutoSize = true;
            this.lblAdressError.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblAdressError.ForeColor = System.Drawing.Color.Red;
            this.lblAdressError.Location = new System.Drawing.Point(131, 48);
            this.lblAdressError.Name = "lblAdressError";
            this.lblAdressError.Size = new System.Drawing.Size(36, 17);
            this.lblAdressError.TabIndex = 12;
            this.lblAdressError.Text = "Error";
            this.lblAdressError.Visible = false;
            // 
            // txtbxAdress
            // 
            this.txtbxAdress.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txtbxAdress.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtbxAdress.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtbxAdress.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtbxAdress.HintForeColor = System.Drawing.Color.Empty;
            this.txtbxAdress.HintText = "Your Adress";
            this.txtbxAdress.isPassword = false;
            this.txtbxAdress.LineFocusedColor = System.Drawing.Color.Teal;
            this.txtbxAdress.LineIdleColor = System.Drawing.Color.Teal;
            this.txtbxAdress.LineMouseHoverColor = System.Drawing.Color.Teal;
            this.txtbxAdress.LineThickness = 3;
            this.txtbxAdress.Location = new System.Drawing.Point(32, 62);
            this.txtbxAdress.Margin = new System.Windows.Forms.Padding(4);
            this.txtbxAdress.Name = "txtbxAdress";
            this.txtbxAdress.Size = new System.Drawing.Size(297, 31);
            this.txtbxAdress.TabIndex = 11;
            this.txtbxAdress.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Teal;
            this.label12.Location = new System.Drawing.Point(28, 45);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(105, 21);
            this.label12.TabIndex = 10;
            this.label12.Text = "New Adress:";
            // 
            // CancelButton
            // 
            this.CancelButton.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.CancelButton.Image = ((System.Drawing.Image)(resources.GetObject("CancelButton.Image")));
            this.CancelButton.ImageActive = null;
            this.CancelButton.Location = new System.Drawing.Point(202, 233);
            this.CancelButton.Margin = new System.Windows.Forms.Padding(2);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(45, 49);
            this.CancelButton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.CancelButton.TabIndex = 24;
            this.CancelButton.TabStop = false;
            this.CancelButton.Zoom = 10;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // ConfirmButton
            // 
            this.ConfirmButton.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.ConfirmButton.Image = ((System.Drawing.Image)(resources.GetObject("ConfirmButton.Image")));
            this.ConfirmButton.ImageActive = null;
            this.ConfirmButton.Location = new System.Drawing.Point(96, 233);
            this.ConfirmButton.Margin = new System.Windows.Forms.Padding(2);
            this.ConfirmButton.Name = "ConfirmButton";
            this.ConfirmButton.Size = new System.Drawing.Size(45, 49);
            this.ConfirmButton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ConfirmButton.TabIndex = 23;
            this.ConfirmButton.TabStop = false;
            this.ConfirmButton.Zoom = 10;
            this.ConfirmButton.Click += new System.EventHandler(this.ConfirmButton_Click);
            // 
            // ChangeAdressWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.ClientSize = new System.Drawing.Size(357, 305);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.ConfirmButton);
            this.Controls.Add(this.lblAdressError);
            this.Controls.Add(this.txtbxAdress);
            this.Controls.Add(this.label12);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ChangeAdressWindow";
            this.Text = "ChangeAdressWindows";
            ((System.ComponentModel.ISupportInitialize)(this.CancelButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConfirmButton)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Bunifu.Framework.UI.BunifuElipse bunifuElipse2;
        private System.Windows.Forms.Label lblAdressError;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtbxAdress;
        private System.Windows.Forms.Label label12;
        private Bunifu.Framework.UI.BunifuImageButton CancelButton;
        public Bunifu.Framework.UI.BunifuImageButton ConfirmButton;
    }
}