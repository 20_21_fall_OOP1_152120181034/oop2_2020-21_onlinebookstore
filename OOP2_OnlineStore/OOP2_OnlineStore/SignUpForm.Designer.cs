﻿namespace OOP_Project
{
    partial class SignUpForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SignUpForm));
            this.panelRight = new System.Windows.Forms.Panel();
            this.lblUserNameError = new System.Windows.Forms.Label();
            this.lblFirstNameError = new System.Windows.Forms.Label();
            this.lblSignUpMessage = new System.Windows.Forms.Label();
            this.lblchckBoxError = new System.Windows.Forms.Label();
            this.lblAdressError = new System.Windows.Forms.Label();
            this.lblEmailError = new System.Windows.Forms.Label();
            this.lblSecondPasswordError = new System.Windows.Forms.Label();
            this.lblFirstPasswordError = new System.Windows.Forms.Label();
            this.lblLastNameError = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.lblSignIn = new System.Windows.Forms.Label();
            this.chckbxAgree = new Bunifu.Framework.UI.BunifuCheckbox();
            this.btnSignUp = new System.Windows.Forms.Button();
            this.txtbxAdress = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.txtbxEmail = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label7 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtbxPasswordTwo = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtbxPasswordOne = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtbxUserName = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.txtbxLastName = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.txtbxFirstName = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblExit = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.bunifuDragControl1 = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.panelLeft = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panelRight.SuspendLayout();
            this.panelLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelRight
            // 
            this.panelRight.Controls.Add(this.lblUserNameError);
            this.panelRight.Controls.Add(this.lblFirstNameError);
            this.panelRight.Controls.Add(this.lblSignUpMessage);
            this.panelRight.Controls.Add(this.lblchckBoxError);
            this.panelRight.Controls.Add(this.lblAdressError);
            this.panelRight.Controls.Add(this.lblEmailError);
            this.panelRight.Controls.Add(this.lblSecondPasswordError);
            this.panelRight.Controls.Add(this.lblFirstPasswordError);
            this.panelRight.Controls.Add(this.lblLastNameError);
            this.panelRight.Controls.Add(this.label14);
            this.panelRight.Controls.Add(this.lblSignIn);
            this.panelRight.Controls.Add(this.chckbxAgree);
            this.panelRight.Controls.Add(this.btnSignUp);
            this.panelRight.Controls.Add(this.txtbxAdress);
            this.panelRight.Controls.Add(this.txtbxEmail);
            this.panelRight.Controls.Add(this.label7);
            this.panelRight.Controls.Add(this.label12);
            this.panelRight.Controls.Add(this.label6);
            this.panelRight.Controls.Add(this.txtbxPasswordTwo);
            this.panelRight.Controls.Add(this.label5);
            this.panelRight.Controls.Add(this.txtbxPasswordOne);
            this.panelRight.Controls.Add(this.label4);
            this.panelRight.Controls.Add(this.txtbxUserName);
            this.panelRight.Controls.Add(this.txtbxLastName);
            this.panelRight.Controls.Add(this.txtbxFirstName);
            this.panelRight.Controls.Add(this.label10);
            this.panelRight.Controls.Add(this.label11);
            this.panelRight.Controls.Add(this.label3);
            this.panelRight.Controls.Add(this.lblExit);
            this.panelRight.Controls.Add(this.label2);
            this.panelRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelRight.Location = new System.Drawing.Point(400, 0);
            this.panelRight.Name = "panelRight";
            this.panelRight.Size = new System.Drawing.Size(400, 720);
            this.panelRight.TabIndex = 1;
            // 
            // lblUserNameError
            // 
            this.lblUserNameError.AutoSize = true;
            this.lblUserNameError.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblUserNameError.ForeColor = System.Drawing.Color.Red;
            this.lblUserNameError.Location = new System.Drawing.Point(142, 99);
            this.lblUserNameError.Name = "lblUserNameError";
            this.lblUserNameError.Size = new System.Drawing.Size(36, 17);
            this.lblUserNameError.TabIndex = 9;
            this.lblUserNameError.Text = "Error";
            // 
            // lblFirstNameError
            // 
            this.lblFirstNameError.AutoSize = true;
            this.lblFirstNameError.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblFirstNameError.ForeColor = System.Drawing.Color.Red;
            this.lblFirstNameError.Location = new System.Drawing.Point(139, 162);
            this.lblFirstNameError.Name = "lblFirstNameError";
            this.lblFirstNameError.Size = new System.Drawing.Size(36, 17);
            this.lblFirstNameError.TabIndex = 9;
            this.lblFirstNameError.Text = "Error";
            // 
            // lblSignUpMessage
            // 
            this.lblSignUpMessage.AutoSize = true;
            this.lblSignUpMessage.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblSignUpMessage.ForeColor = System.Drawing.Color.Green;
            this.lblSignUpMessage.Location = new System.Drawing.Point(84, 582);
            this.lblSignUpMessage.Name = "lblSignUpMessage";
            this.lblSignUpMessage.Size = new System.Drawing.Size(233, 23);
            this.lblSignUpMessage.TabIndex = 9;
            this.lblSignUpMessage.Text = "Registration is successful";
            this.lblSignUpMessage.Visible = false;
            // 
            // lblchckBoxError
            // 
            this.lblchckBoxError.AutoSize = true;
            this.lblchckBoxError.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblchckBoxError.ForeColor = System.Drawing.Color.Red;
            this.lblchckBoxError.Location = new System.Drawing.Point(58, 588);
            this.lblchckBoxError.Name = "lblchckBoxError";
            this.lblchckBoxError.Size = new System.Drawing.Size(36, 17);
            this.lblchckBoxError.TabIndex = 9;
            this.lblchckBoxError.Text = "Error";
            this.lblchckBoxError.Visible = false;
            // 
            // lblAdressError
            // 
            this.lblAdressError.AutoSize = true;
            this.lblAdressError.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblAdressError.ForeColor = System.Drawing.Color.Red;
            this.lblAdressError.Location = new System.Drawing.Point(109, 482);
            this.lblAdressError.Name = "lblAdressError";
            this.lblAdressError.Size = new System.Drawing.Size(36, 17);
            this.lblAdressError.TabIndex = 9;
            this.lblAdressError.Text = "Error";
            // 
            // lblEmailError
            // 
            this.lblEmailError.AutoSize = true;
            this.lblEmailError.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblEmailError.ForeColor = System.Drawing.Color.Red;
            this.lblEmailError.Location = new System.Drawing.Point(99, 418);
            this.lblEmailError.Name = "lblEmailError";
            this.lblEmailError.Size = new System.Drawing.Size(36, 17);
            this.lblEmailError.TabIndex = 9;
            this.lblEmailError.Text = "Error";
            // 
            // lblSecondPasswordError
            // 
            this.lblSecondPasswordError.AutoSize = true;
            this.lblSecondPasswordError.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblSecondPasswordError.ForeColor = System.Drawing.Color.Red;
            this.lblSecondPasswordError.Location = new System.Drawing.Point(197, 354);
            this.lblSecondPasswordError.Name = "lblSecondPasswordError";
            this.lblSecondPasswordError.Size = new System.Drawing.Size(36, 17);
            this.lblSecondPasswordError.TabIndex = 9;
            this.lblSecondPasswordError.Text = "Error";
            // 
            // lblFirstPasswordError
            // 
            this.lblFirstPasswordError.AutoSize = true;
            this.lblFirstPasswordError.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblFirstPasswordError.ForeColor = System.Drawing.Color.Red;
            this.lblFirstPasswordError.Location = new System.Drawing.Point(130, 290);
            this.lblFirstPasswordError.Name = "lblFirstPasswordError";
            this.lblFirstPasswordError.Size = new System.Drawing.Size(36, 17);
            this.lblFirstPasswordError.TabIndex = 9;
            this.lblFirstPasswordError.Text = "Error";
            // 
            // lblLastNameError
            // 
            this.lblLastNameError.AutoSize = true;
            this.lblLastNameError.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblLastNameError.ForeColor = System.Drawing.Color.Red;
            this.lblLastNameError.Location = new System.Drawing.Point(143, 226);
            this.lblLastNameError.Name = "lblLastNameError";
            this.lblLastNameError.Size = new System.Drawing.Size(36, 17);
            this.lblLastNameError.TabIndex = 9;
            this.lblLastNameError.Text = "Error";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(57, 672);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(222, 21);
            this.label14.TabIndex = 5;
            this.label14.Text = "Already have an account?";
            // 
            // lblSignIn
            // 
            this.lblSignIn.AutoSize = true;
            this.lblSignIn.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSignIn.ForeColor = System.Drawing.Color.Teal;
            this.lblSignIn.Location = new System.Drawing.Point(275, 672);
            this.lblSignIn.Name = "lblSignIn";
            this.lblSignIn.Size = new System.Drawing.Size(59, 21);
            this.lblSignIn.TabIndex = 5;
            this.lblSignIn.Text = "Sign in";
            this.lblSignIn.Click += new System.EventHandler(this.lblSignIn_Click);
            this.lblSignIn.MouseLeave += new System.EventHandler(this.lblSignIn_MouseLeave);
            this.lblSignIn.MouseHover += new System.EventHandler(this.lblSignIn_MouseHover);
            // 
            // chckbxAgree
            // 
            this.chckbxAgree.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(135)))), ((int)(((byte)(140)))));
            this.chckbxAgree.ChechedOffColor = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(135)))), ((int)(((byte)(140)))));
            this.chckbxAgree.Checked = false;
            this.chckbxAgree.CheckedOnColor = System.Drawing.Color.Teal;
            this.chckbxAgree.ForeColor = System.Drawing.Color.White;
            this.chckbxAgree.Location = new System.Drawing.Point(69, 553);
            this.chckbxAgree.Margin = new System.Windows.Forms.Padding(4);
            this.chckbxAgree.Name = "chckbxAgree";
            this.chckbxAgree.Size = new System.Drawing.Size(20, 20);
            this.chckbxAgree.TabIndex = 7;
            // 
            // btnSignUp
            // 
            this.btnSignUp.BackColor = System.Drawing.Color.Teal;
            this.btnSignUp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSignUp.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSignUp.ForeColor = System.Drawing.Color.White;
            this.btnSignUp.Location = new System.Drawing.Point(50, 613);
            this.btnSignUp.Name = "btnSignUp";
            this.btnSignUp.Size = new System.Drawing.Size(297, 34);
            this.btnSignUp.TabIndex = 8;
            this.btnSignUp.Text = "Sign Up";
            this.btnSignUp.UseVisualStyleBackColor = false;
            this.btnSignUp.Click += new System.EventHandler(this.btnSignUp_Click);
            // 
            // txtbxAdress
            // 
            this.txtbxAdress.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.txtbxAdress.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtbxAdress.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtbxAdress.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtbxAdress.HintForeColor = System.Drawing.Color.Empty;
            this.txtbxAdress.HintText = "Your Adress";
            this.txtbxAdress.isPassword = false;
            this.txtbxAdress.LineFocusedColor = System.Drawing.Color.Teal;
            this.txtbxAdress.LineIdleColor = System.Drawing.Color.Teal;
            this.txtbxAdress.LineMouseHoverColor = System.Drawing.Color.Teal;
            this.txtbxAdress.LineThickness = 3;
            this.txtbxAdress.Location = new System.Drawing.Point(50, 496);
            this.txtbxAdress.Margin = new System.Windows.Forms.Padding(4);
            this.txtbxAdress.Name = "txtbxAdress";
            this.txtbxAdress.Size = new System.Drawing.Size(297, 31);
            this.txtbxAdress.TabIndex = 6;
            this.txtbxAdress.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // txtbxEmail
            // 
            this.txtbxEmail.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.txtbxEmail.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtbxEmail.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtbxEmail.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtbxEmail.HintForeColor = System.Drawing.Color.Empty;
            this.txtbxEmail.HintText = "someone@example.com";
            this.txtbxEmail.isPassword = false;
            this.txtbxEmail.LineFocusedColor = System.Drawing.Color.Teal;
            this.txtbxEmail.LineIdleColor = System.Drawing.Color.Teal;
            this.txtbxEmail.LineMouseHoverColor = System.Drawing.Color.Teal;
            this.txtbxEmail.LineThickness = 3;
            this.txtbxEmail.Location = new System.Drawing.Point(50, 432);
            this.txtbxEmail.Margin = new System.Windows.Forms.Padding(4);
            this.txtbxEmail.Name = "txtbxEmail";
            this.txtbxEmail.Size = new System.Drawing.Size(297, 31);
            this.txtbxEmail.TabIndex = 5;
            this.txtbxEmail.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Teal;
            this.label7.Location = new System.Drawing.Point(95, 553);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(239, 21);
            this.label7.TabIndex = 0;
            this.label7.Text = "I Agree Terms and Conditions";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Teal;
            this.label12.Location = new System.Drawing.Point(46, 479);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(65, 21);
            this.label12.TabIndex = 0;
            this.label12.Text = "Adress:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Teal;
            this.label6.Location = new System.Drawing.Point(46, 415);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 21);
            this.label6.TabIndex = 0;
            this.label6.Text = "Email:";
            // 
            // txtbxPasswordTwo
            // 
            this.txtbxPasswordTwo.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.txtbxPasswordTwo.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtbxPasswordTwo.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtbxPasswordTwo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtbxPasswordTwo.HintForeColor = System.Drawing.Color.Empty;
            this.txtbxPasswordTwo.HintText = "Password";
            this.txtbxPasswordTwo.isPassword = true;
            this.txtbxPasswordTwo.LineFocusedColor = System.Drawing.Color.Teal;
            this.txtbxPasswordTwo.LineIdleColor = System.Drawing.Color.Teal;
            this.txtbxPasswordTwo.LineMouseHoverColor = System.Drawing.Color.Teal;
            this.txtbxPasswordTwo.LineThickness = 3;
            this.txtbxPasswordTwo.Location = new System.Drawing.Point(50, 368);
            this.txtbxPasswordTwo.Margin = new System.Windows.Forms.Padding(4);
            this.txtbxPasswordTwo.Name = "txtbxPasswordTwo";
            this.txtbxPasswordTwo.Size = new System.Drawing.Size(297, 31);
            this.txtbxPasswordTwo.TabIndex = 4;
            this.txtbxPasswordTwo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtbxPasswordTwo.OnValueChanged += new System.EventHandler(this.txtbxPasswordTwo_OnValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Teal;
            this.label5.Location = new System.Drawing.Point(46, 351);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(156, 21);
            this.label5.TabIndex = 0;
            this.label5.Text = "Re-Enter Password:";
            // 
            // txtbxPasswordOne
            // 
            this.txtbxPasswordOne.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.txtbxPasswordOne.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtbxPasswordOne.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtbxPasswordOne.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtbxPasswordOne.HintForeColor = System.Drawing.Color.Empty;
            this.txtbxPasswordOne.HintText = "Password";
            this.txtbxPasswordOne.isPassword = true;
            this.txtbxPasswordOne.LineFocusedColor = System.Drawing.Color.Teal;
            this.txtbxPasswordOne.LineIdleColor = System.Drawing.Color.Teal;
            this.txtbxPasswordOne.LineMouseHoverColor = System.Drawing.Color.Teal;
            this.txtbxPasswordOne.LineThickness = 3;
            this.txtbxPasswordOne.Location = new System.Drawing.Point(50, 304);
            this.txtbxPasswordOne.Margin = new System.Windows.Forms.Padding(4);
            this.txtbxPasswordOne.Name = "txtbxPasswordOne";
            this.txtbxPasswordOne.Size = new System.Drawing.Size(297, 31);
            this.txtbxPasswordOne.TabIndex = 3;
            this.txtbxPasswordOne.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtbxPasswordOne.OnValueChanged += new System.EventHandler(this.txtbxPasswordOne_OnValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Teal;
            this.label4.Location = new System.Drawing.Point(46, 287);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 21);
            this.label4.TabIndex = 0;
            this.label4.Text = "Password:";
            // 
            // txtbxUserName
            // 
            this.txtbxUserName.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.txtbxUserName.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtbxUserName.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtbxUserName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtbxUserName.HintForeColor = System.Drawing.Color.Empty;
            this.txtbxUserName.HintText = "User Name";
            this.txtbxUserName.isPassword = false;
            this.txtbxUserName.LineFocusedColor = System.Drawing.Color.Teal;
            this.txtbxUserName.LineIdleColor = System.Drawing.Color.Teal;
            this.txtbxUserName.LineMouseHoverColor = System.Drawing.Color.Teal;
            this.txtbxUserName.LineThickness = 3;
            this.txtbxUserName.Location = new System.Drawing.Point(50, 112);
            this.txtbxUserName.Margin = new System.Windows.Forms.Padding(4);
            this.txtbxUserName.Name = "txtbxUserName";
            this.txtbxUserName.Size = new System.Drawing.Size(297, 31);
            this.txtbxUserName.TabIndex = 0;
            this.txtbxUserName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // txtbxLastName
            // 
            this.txtbxLastName.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.txtbxLastName.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtbxLastName.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtbxLastName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtbxLastName.HintForeColor = System.Drawing.Color.Empty;
            this.txtbxLastName.HintText = "Last Name";
            this.txtbxLastName.isPassword = false;
            this.txtbxLastName.LineFocusedColor = System.Drawing.Color.Teal;
            this.txtbxLastName.LineIdleColor = System.Drawing.Color.Teal;
            this.txtbxLastName.LineMouseHoverColor = System.Drawing.Color.Teal;
            this.txtbxLastName.LineThickness = 3;
            this.txtbxLastName.Location = new System.Drawing.Point(52, 240);
            this.txtbxLastName.Margin = new System.Windows.Forms.Padding(4);
            this.txtbxLastName.Name = "txtbxLastName";
            this.txtbxLastName.Size = new System.Drawing.Size(297, 31);
            this.txtbxLastName.TabIndex = 2;
            this.txtbxLastName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // txtbxFirstName
            // 
            this.txtbxFirstName.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.txtbxFirstName.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtbxFirstName.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtbxFirstName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtbxFirstName.HintForeColor = System.Drawing.Color.Empty;
            this.txtbxFirstName.HintText = "First Name";
            this.txtbxFirstName.isPassword = false;
            this.txtbxFirstName.LineFocusedColor = System.Drawing.Color.Teal;
            this.txtbxFirstName.LineIdleColor = System.Drawing.Color.Teal;
            this.txtbxFirstName.LineMouseHoverColor = System.Drawing.Color.Teal;
            this.txtbxFirstName.LineThickness = 3;
            this.txtbxFirstName.Location = new System.Drawing.Point(50, 176);
            this.txtbxFirstName.Margin = new System.Windows.Forms.Padding(4);
            this.txtbxFirstName.Name = "txtbxFirstName";
            this.txtbxFirstName.Size = new System.Drawing.Size(297, 31);
            this.txtbxFirstName.TabIndex = 1;
            this.txtbxFirstName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Teal;
            this.label10.Location = new System.Drawing.Point(46, 95);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(98, 21);
            this.label10.TabIndex = 0;
            this.label10.Text = "User Name:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Teal;
            this.label11.Location = new System.Drawing.Point(48, 223);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(97, 21);
            this.label11.TabIndex = 0;
            this.label11.Text = "Last Name:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Teal;
            this.label3.Location = new System.Drawing.Point(46, 159);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 21);
            this.label3.TabIndex = 0;
            this.label3.Text = "First Name:";
            // 
            // lblExit
            // 
            this.lblExit.AutoSize = true;
            this.lblExit.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(26)))), ((int)(((byte)(74)))));
            this.lblExit.Location = new System.Drawing.Point(363, 9);
            this.lblExit.Name = "lblExit";
            this.lblExit.Size = new System.Drawing.Size(25, 30);
            this.lblExit.TabIndex = 0;
            this.lblExit.Text = "x";
            this.lblExit.Click += new System.EventHandler(this.lblExit_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Teal;
            this.label2.Location = new System.Drawing.Point(45, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 30);
            this.label2.TabIndex = 0;
            this.label2.Text = "Sign Up";
            // 
            // bunifuElipse1
            // 
            this.bunifuElipse1.ElipseRadius = 25;
            this.bunifuElipse1.TargetControl = this;
            // 
            // bunifuDragControl1
            // 
            this.bunifuDragControl1.Fixed = true;
            this.bunifuDragControl1.Horizontal = true;
            this.bunifuDragControl1.TargetControl = this.panelLeft;
            this.bunifuDragControl1.Vertical = true;
            // 
            // panelLeft
            // 
            this.panelLeft.BackColor = System.Drawing.Color.Teal;
            this.panelLeft.Controls.Add(this.label9);
            this.panelLeft.Controls.Add(this.label1);
            this.panelLeft.Controls.Add(this.pictureBox1);
            this.panelLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelLeft.Location = new System.Drawing.Point(0, 0);
            this.panelLeft.Name = "panelLeft";
            this.panelLeft.Size = new System.Drawing.Size(400, 720);
            this.panelLeft.TabIndex = 0;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(98, 673);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(188, 20);
            this.label9.TabIndex = 1;
            this.label9.Text = "Copyright © ESOGU 2021";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(77, 379);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(292, 60);
            this.label1.TabIndex = 1;
            this.label1.Text = "Best Books, Best Musics,\r\n       Best Magazines";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(93, 125);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(228, 221);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // SignUpForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 720);
            this.Controls.Add(this.panelRight);
            this.Controls.Add(this.panelLeft);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "SignUpForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SignUpForm";
            this.panelRight.ResumeLayout(false);
            this.panelRight.PerformLayout();
            this.panelLeft.ResumeLayout(false);
            this.panelLeft.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panelRight;
        private System.Windows.Forms.Label label2;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtbxPasswordOne;
        private System.Windows.Forms.Label lblExit;
        private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
        private Bunifu.Framework.UI.BunifuDragControl bunifuDragControl1;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtbxUserName;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label4;
        private Bunifu.Framework.UI.BunifuCheckbox chckbxAgree;
        private System.Windows.Forms.Button btnSignUp;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtbxAdress;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtbxEmail;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label6;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtbxPasswordTwo;
        private System.Windows.Forms.Label label5;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtbxLastName;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtbxFirstName;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panelLeft;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblSignIn;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lblUserNameError;
        private System.Windows.Forms.Label lblFirstNameError;
        private System.Windows.Forms.Label lblAdressError;
        private System.Windows.Forms.Label lblEmailError;
        private System.Windows.Forms.Label lblSecondPasswordError;
        private System.Windows.Forms.Label lblFirstPasswordError;
        private System.Windows.Forms.Label lblLastNameError;
        private System.Windows.Forms.Label lblSignUpMessage;
        private System.Windows.Forms.Label lblchckBoxError;
    }
}

