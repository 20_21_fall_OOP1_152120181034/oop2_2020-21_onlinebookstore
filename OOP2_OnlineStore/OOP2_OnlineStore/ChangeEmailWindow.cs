﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EntityStore;
using DTLStore;
using LogicLayerStore;

namespace OOP_Project
{
    public partial class ChangeEmailWindow : Form
    {
        EntityUser currentUser;
        public ChangeEmailWindow(EntityUser User)
        {
            InitializeComponent();
            currentUser = User;
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void ConfirmButton_Click(object sender, EventArgs e)
        {
            makeErrorsInvisible();
            bool flag = false;
            if (txtbxEmail.Text == string.Empty)
            {
                txtbxEmail.LineIdleColor = Color.Red;
                txtbxEmail.LineFocusedColor = Color.Red;
                txtbxEmail.LineMouseHoverColor = Color.Red;
                lblEmailError.Text = "Email cannot be left blank";
                lblEmailError.Visible = true;
                flag = true;
            }
            if (txtbxReEmail.Text == string.Empty)
            {
                txtbxReEmail.LineIdleColor = Color.Red;
                txtbxReEmail.LineFocusedColor = Color.Red;
                txtbxReEmail.LineMouseHoverColor = Color.Red;
                lblReEmailError.Text = "Re-Email cannot be left blank";
                lblReEmailError.Visible = true;
                flag = true;
            }
            if (!flag)
            {
                if (txtbxEmail.Text == txtbxReEmail.Text)
                {
                    currentUser.UserEmail = txtbxEmail.Text;
                    LLUser.LLUserUpdate(currentUser);
                    this.Close();
                }
                else
                {
                    txtbxReEmail.LineIdleColor = Color.Red;
                    txtbxReEmail.LineFocusedColor = Color.Red;
                    txtbxReEmail.LineMouseHoverColor = Color.Red;
                    lblReEmailError.Text = "Re-Email doesn't match";
                    lblReEmailError.Visible = true;
                    txtbxEmail.LineIdleColor = Color.Red;
                    txtbxEmail.LineFocusedColor = Color.Red;
                    txtbxEmail.LineMouseHoverColor = Color.Red;
                    lblEmailError.Text = "Email doesn't match";
                    lblEmailError.Visible = true;
                }
                
            }
        }
        private void makeErrorsInvisible()
        {
            txtbxReEmail.LineIdleColor = Color.Teal;
            txtbxReEmail.LineFocusedColor = Color.Teal;
            txtbxReEmail.LineMouseHoverColor = Color.Teal;
            lblReEmailError.Visible = false;
            txtbxEmail.LineIdleColor = Color.Teal;
            txtbxEmail.LineFocusedColor = Color.Teal;
            txtbxEmail.LineMouseHoverColor = Color.Teal;
            lblEmailError.Visible = false;
        }
    }
}
