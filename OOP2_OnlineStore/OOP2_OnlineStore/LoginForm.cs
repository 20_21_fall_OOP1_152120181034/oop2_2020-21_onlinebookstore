﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicLayerStore;
using EntityStore;
using DTLStore;
namespace OOP_Project
{
    /*! 
    *  \brief     This is a data access class.  
    *  \details   This class allows us to access the Magazines in the database.
    *  \author    Alperen Bişkin
    *  \date      04.06.2021
    */
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
        }
        /// <summary> 
        /// Exit button
        /// </summary>
        private void lblExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        private void SignUpButton_MouseHover(object sender, EventArgs e)
        {
            SignUpButton.ForeColor = Color.FromArgb(0, 64, 64);
        }

        private void SignUpButton_MouseLeave(object sender, EventArgs e)
        {
            SignUpButton.ForeColor = Color.Teal;
        }

        private void SignUpButton_MouseClick(object sender, MouseEventArgs e)
        {
            SignUpForm signUpForm = new SignUpForm();
            this.Hide();
            signUpForm.ShowDialog();
            this.Close();
        }
        /// <summary> 
        /// Login button
        /// </summary>
        private void btnLogin_Click(object sender, EventArgs e)
        {
            lblUserNameError.Text = "";
                        List <EntityUser> Users = LLUser.LLUserList();
            bool flag = true; ;
            foreach (var item in Users)
            {
                if (txtboxUserName.Text == item.UserName && item.encryptPassword(txtboxPassword.Text) == item.UserPassword)
                {
                    DashBoardForm frmDashBorad = new DashBoardForm(item.UserId);
                    this.Hide();
                    frmDashBorad.ShowDialog();
                    this.Close();
                    flag = false;
                    break;
                }
                if (flag)
                    lblUserNameError.Text = "User Name and Password doesn't match!";
            }
        }

        private void txtboxPassword_OnValueChanged(object sender, EventArgs e)
        {
            txtboxPassword.isPassword = true;
        }

    }
}
