﻿
namespace OOP_Project
{
    partial class BooksDetailForm
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BooksDetailForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.AddToCartButton = new Bunifu.Framework.UI.BunifuFlatButton();
            this.numericValueQuantity = new System.Windows.Forms.NumericUpDown();
            this.bunifuCustomLabel2 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel1 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.BookPriceLabel = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.BookPagesLabel = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.BookISBNLabel = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.BookPublisherLabel = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.BookAuthorLabel = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.BookNameLabel = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel3 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel5 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel4 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.lblInfo = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.pctrBoxInfo = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericValueQuantity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctrBoxInfo)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Teal;
            this.panel1.Controls.Add(this.AddToCartButton);
            this.panel1.Controls.Add(this.numericValueQuantity);
            this.panel1.Controls.Add(this.bunifuCustomLabel2);
            this.panel1.Controls.Add(this.bunifuCustomLabel1);
            this.panel1.Controls.Add(this.BookPriceLabel);
            this.panel1.Controls.Add(this.BookPagesLabel);
            this.panel1.Controls.Add(this.BookISBNLabel);
            this.panel1.Controls.Add(this.BookPublisherLabel);
            this.panel1.Controls.Add(this.BookAuthorLabel);
            this.panel1.Controls.Add(this.BookNameLabel);
            this.panel1.Controls.Add(this.bunifuCustomLabel3);
            this.panel1.Controls.Add(this.bunifuCustomLabel5);
            this.panel1.Controls.Add(this.bunifuCustomLabel4);
            this.panel1.Controls.Add(this.lblInfo);
            this.panel1.Controls.Add(this.pctrBoxInfo);
            this.panel1.Location = new System.Drawing.Point(5, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(250, 250);
            this.panel1.TabIndex = 1;
            // 
            // AddToCartButton
            // 
            this.AddToCartButton.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.AddToCartButton.BackColor = System.Drawing.Color.Teal;
            this.AddToCartButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.AddToCartButton.BorderRadius = 0;
            this.AddToCartButton.ButtonText = "Add to Cart";
            this.AddToCartButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.AddToCartButton.DisabledColor = System.Drawing.Color.Gray;
            this.AddToCartButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.AddToCartButton.ForeColor = System.Drawing.SystemColors.Control;
            this.AddToCartButton.Iconcolor = System.Drawing.Color.Transparent;
            this.AddToCartButton.Iconimage = ((System.Drawing.Image)(resources.GetObject("AddToCartButton.Iconimage")));
            this.AddToCartButton.Iconimage_right = null;
            this.AddToCartButton.Iconimage_right_Selected = null;
            this.AddToCartButton.Iconimage_Selected = null;
            this.AddToCartButton.IconMarginLeft = 0;
            this.AddToCartButton.IconMarginRight = 0;
            this.AddToCartButton.IconRightVisible = true;
            this.AddToCartButton.IconRightZoom = 0D;
            this.AddToCartButton.IconVisible = true;
            this.AddToCartButton.IconZoom = 60D;
            this.AddToCartButton.IsTab = false;
            this.AddToCartButton.Location = new System.Drawing.Point(95, 209);
            this.AddToCartButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.AddToCartButton.Name = "AddToCartButton";
            this.AddToCartButton.Normalcolor = System.Drawing.Color.Teal;
            this.AddToCartButton.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.AddToCartButton.OnHoverTextColor = System.Drawing.SystemColors.Control;
            this.AddToCartButton.selected = false;
            this.AddToCartButton.Size = new System.Drawing.Size(127, 29);
            this.AddToCartButton.TabIndex = 3;
            this.AddToCartButton.Tag = "AddToCart";
            this.AddToCartButton.Text = "Add to Cart";
            this.AddToCartButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.AddToCartButton.Textcolor = System.Drawing.SystemColors.Control;
            this.AddToCartButton.TextFont = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            // 
            // numericValueQuantity
            // 
            this.numericValueQuantity.BackColor = System.Drawing.Color.Teal;
            this.numericValueQuantity.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.numericValueQuantity.ForeColor = System.Drawing.SystemColors.Control;
            this.numericValueQuantity.Location = new System.Drawing.Point(30, 213);
            this.numericValueQuantity.Margin = new System.Windows.Forms.Padding(2);
            this.numericValueQuantity.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericValueQuantity.Name = "numericValueQuantity";
            this.numericValueQuantity.Size = new System.Drawing.Size(32, 22);
            this.numericValueQuantity.TabIndex = 2;
            this.numericValueQuantity.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // bunifuCustomLabel2
            // 
            this.bunifuCustomLabel2.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.bunifuCustomLabel2.Location = new System.Drawing.Point(14, 161);
            this.bunifuCustomLabel2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.bunifuCustomLabel2.Name = "bunifuCustomLabel2";
            this.bunifuCustomLabel2.Size = new System.Drawing.Size(98, 21);
            this.bunifuCustomLabel2.TabIndex = 1;
            this.bunifuCustomLabel2.Text = "ISBN Number:";
            // 
            // bunifuCustomLabel1
            // 
            this.bunifuCustomLabel1.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.bunifuCustomLabel1.Location = new System.Drawing.Point(14, 140);
            this.bunifuCustomLabel1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.bunifuCustomLabel1.Name = "bunifuCustomLabel1";
            this.bunifuCustomLabel1.Size = new System.Drawing.Size(72, 21);
            this.bunifuCustomLabel1.TabIndex = 1;
            this.bunifuCustomLabel1.Text = "Publisher:";
            // 
            // BookPriceLabel
            // 
            this.BookPriceLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.BookPriceLabel.ForeColor = System.Drawing.SystemColors.Control;
            this.BookPriceLabel.Location = new System.Drawing.Point(167, 181);
            this.BookPriceLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.BookPriceLabel.Name = "BookPriceLabel";
            this.BookPriceLabel.Size = new System.Drawing.Size(81, 21);
            this.BookPriceLabel.TabIndex = 1;
            // 
            // BookPagesLabel
            // 
            this.BookPagesLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.BookPagesLabel.ForeColor = System.Drawing.SystemColors.Control;
            this.BookPagesLabel.Location = new System.Drawing.Point(67, 181);
            this.BookPagesLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.BookPagesLabel.Name = "BookPagesLabel";
            this.BookPagesLabel.Size = new System.Drawing.Size(57, 21);
            this.BookPagesLabel.TabIndex = 1;
            // 
            // BookISBNLabel
            // 
            this.BookISBNLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.BookISBNLabel.ForeColor = System.Drawing.SystemColors.Control;
            this.BookISBNLabel.Location = new System.Drawing.Point(117, 161);
            this.BookISBNLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.BookISBNLabel.Name = "BookISBNLabel";
            this.BookISBNLabel.Size = new System.Drawing.Size(131, 21);
            this.BookISBNLabel.TabIndex = 1;
            // 
            // BookPublisherLabel
            // 
            this.BookPublisherLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.BookPublisherLabel.ForeColor = System.Drawing.SystemColors.Control;
            this.BookPublisherLabel.Location = new System.Drawing.Point(86, 140);
            this.BookPublisherLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.BookPublisherLabel.Name = "BookPublisherLabel";
            this.BookPublisherLabel.Size = new System.Drawing.Size(136, 21);
            this.BookPublisherLabel.TabIndex = 1;
            // 
            // BookAuthorLabel
            // 
            this.BookAuthorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.BookAuthorLabel.ForeColor = System.Drawing.SystemColors.Control;
            this.BookAuthorLabel.Location = new System.Drawing.Point(65, 119);
            this.BookAuthorLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.BookAuthorLabel.Name = "BookAuthorLabel";
            this.BookAuthorLabel.Size = new System.Drawing.Size(181, 21);
            this.BookAuthorLabel.TabIndex = 1;
            // 
            // BookNameLabel
            // 
            this.BookNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.BookNameLabel.ForeColor = System.Drawing.SystemColors.Control;
            this.BookNameLabel.Location = new System.Drawing.Point(112, 98);
            this.BookNameLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.BookNameLabel.Name = "BookNameLabel";
            this.BookNameLabel.Size = new System.Drawing.Size(136, 21);
            this.BookNameLabel.TabIndex = 1;
            // 
            // bunifuCustomLabel3
            // 
            this.bunifuCustomLabel3.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.bunifuCustomLabel3.Location = new System.Drawing.Point(14, 98);
            this.bunifuCustomLabel3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.bunifuCustomLabel3.Name = "bunifuCustomLabel3";
            this.bunifuCustomLabel3.Size = new System.Drawing.Size(96, 21);
            this.bunifuCustomLabel3.TabIndex = 1;
            this.bunifuCustomLabel3.Text = "Book\'s Name:";
            // 
            // bunifuCustomLabel5
            // 
            this.bunifuCustomLabel5.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.bunifuCustomLabel5.Location = new System.Drawing.Point(122, 182);
            this.bunifuCustomLabel5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.bunifuCustomLabel5.Name = "bunifuCustomLabel5";
            this.bunifuCustomLabel5.Size = new System.Drawing.Size(44, 21);
            this.bunifuCustomLabel5.TabIndex = 1;
            this.bunifuCustomLabel5.Text = "Price:";
            // 
            // bunifuCustomLabel4
            // 
            this.bunifuCustomLabel4.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.bunifuCustomLabel4.Location = new System.Drawing.Point(14, 182);
            this.bunifuCustomLabel4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.bunifuCustomLabel4.Name = "bunifuCustomLabel4";
            this.bunifuCustomLabel4.Size = new System.Drawing.Size(51, 21);
            this.bunifuCustomLabel4.TabIndex = 1;
            this.bunifuCustomLabel4.Text = "Pages:";
            // 
            // lblInfo
            // 
            this.lblInfo.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblInfo.Location = new System.Drawing.Point(14, 119);
            this.lblInfo.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(52, 21);
            this.lblInfo.TabIndex = 1;
            this.lblInfo.Text = "Author:";
            // 
            // pctrBoxInfo
            // 
            this.pctrBoxInfo.Location = new System.Drawing.Point(70, 2);
            this.pctrBoxInfo.Margin = new System.Windows.Forms.Padding(2);
            this.pctrBoxInfo.Name = "pctrBoxInfo";
            this.pctrBoxInfo.Size = new System.Drawing.Size(103, 94);
            this.pctrBoxInfo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pctrBoxInfo.TabIndex = 0;
            this.pctrBoxInfo.TabStop = false;
            // 
            // BooksDetailForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel1);
            this.Name = "BooksDetailForm";
            this.Size = new System.Drawing.Size(260, 250);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericValueQuantity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctrBoxInfo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        public Bunifu.Framework.UI.BunifuFlatButton AddToCartButton;
        public System.Windows.Forms.NumericUpDown numericValueQuantity;
        public Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel2;
        public Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel1;
        public Bunifu.Framework.UI.BunifuCustomLabel BookPriceLabel;
        public Bunifu.Framework.UI.BunifuCustomLabel BookPagesLabel;
        public Bunifu.Framework.UI.BunifuCustomLabel BookISBNLabel;
        public Bunifu.Framework.UI.BunifuCustomLabel BookPublisherLabel;
        public Bunifu.Framework.UI.BunifuCustomLabel BookAuthorLabel;
        public Bunifu.Framework.UI.BunifuCustomLabel BookNameLabel;
        public Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel3;
        public Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel5;
        public Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel4;
        public Bunifu.Framework.UI.BunifuCustomLabel lblInfo;
        public System.Windows.Forms.PictureBox pctrBoxInfo;
    }
}
