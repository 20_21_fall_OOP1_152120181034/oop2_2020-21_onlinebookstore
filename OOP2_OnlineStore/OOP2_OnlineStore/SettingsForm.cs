﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EntityStore;
using LogicLayerStore;
using DTLStore;

namespace OOP_Project
{
    public partial class SettingsForm : Form
    {
        EntityUser currentUser = new EntityUser();
        private int currentUserID;
        private string currentUserName;
        List<EntityUser> users;
        List<EntityUserLog> userLogs;
        public SettingsForm(int currentUserID,string currentUserName)
        {
            InitializeComponent();
            this.currentUserID =currentUserID;
            this.currentUserName = currentUserName;
        }
        /// <summary> 
        /// When form opened, necessary values assign
        /// </summary>
        private void SettingsForm_Load(object sender, EventArgs e)
        {
            users = LLUser.LLUserList();
            userLogs = LLUserLog.userLogList(currentUserID);
            foreach (var item in users)
            {
                if (currentUserID==item.UserId)
                {
                    lblEmail.Text = item.UserEmail;
                    lblFirstName.Text = item.UserFirstName;
                    lblLastName.Text = item.UserLastName;
                    lblUserName.Text = item.UserName;
                    msgBoxAdress.Text = item.UserAdress;
                    currentUser = item;
                    break;
                }
            }
            foreach(var item in userLogs)
            {
                string[] arr = new string[2];
                arr[0] = item.CustomerEvent;
                arr[1] = item.CustomerEventTimeStampt.ToString("dd / MM / yyyy HH: mm:ss");
                ListViewItem lstitem = new ListViewItem(arr);
                lstviewLogs.Items.Add(lstitem);
            }
            lblUserNameLog.Text = currentUserName + " " + lblUserNameLog.Text;
        }
        /// <summary> 
        /// Password change button
        /// </summary>
        private void btnPasswordChange_Click(object sender, EventArgs e)
        {
            ChangePasswordWindow frm = new ChangePasswordWindow(currentUser);
            frm.ConfirmButton.Click += delegate
            {
                sendLog("User changed password information");
                SettingsForm_Load(sender, e);
            };
            frm.Show();
        }
        /// <summary> 
        /// Email change button
        /// </summary>
        private void btnEmailChange_Click(object sender, EventArgs e)
        {
            ChangeEmailWindow frm = new ChangeEmailWindow(currentUser);
            frm.ConfirmButton.Click += delegate {
                sendLog("User changed email information");
                SettingsForm_Load(sender, e);
            };
            frm.Show();

        }
        /// <summary> 
        /// Adress change button
        /// </summary>
        private void btnAdressChange_Click(object sender, EventArgs e)
        {
            ChangeAdressWindow frm = new ChangeAdressWindow(currentUser);
            frm.ConfirmButton.Click += delegate {
                sendLog("User changed adress information");
                SettingsForm_Load(sender, e);
            };
            frm.Show();
        }
        /// <summary> 
        /// Send the users log to database
        /// </summary>
        private void sendLog(string customerEvent)
        {
            EntityUserLog userLog = new EntityUserLog();
            userLog.CustomerId = this.currentUserID;
            userLog.CustomerName = this.currentUserName;
            userLog.CustomerEvent = customerEvent;
            userLog.CustomerEventTimeStampt = DateTime.Now;
            LLUserLog.UserLogAdd(userLog);
        }
    }
}
