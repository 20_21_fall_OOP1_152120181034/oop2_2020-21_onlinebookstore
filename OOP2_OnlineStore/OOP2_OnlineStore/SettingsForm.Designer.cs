﻿
namespace OOP_Project
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.lblPassword = new System.Windows.Forms.Label();
            this.lblFirstName = new System.Windows.Forms.Label();
            this.lblLastName = new System.Windows.Forms.Label();
            this.lblEmail = new System.Windows.Forms.Label();
            this.msgBoxAdress = new System.Windows.Forms.TextBox();
            this.panelCart = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lstviewLogs = new System.Windows.Forms.ListView();
            this.UserEvent = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Date = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblUserNameLog = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.btnPasswordChange = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnAdressChange = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnEmailChange = new Bunifu.Framework.UI.BunifuFlatButton();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panelCart.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(11, 152);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(313, 313);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Teal;
            this.label10.Location = new System.Drawing.Point(335, 179);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(98, 21);
            this.label10.TabIndex = 1;
            this.label10.Text = "User Name:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Teal;
            this.label1.Location = new System.Drawing.Point(335, 271);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 21);
            this.label1.TabIndex = 1;
            this.label1.Text = "First Name:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Teal;
            this.label2.Location = new System.Drawing.Point(335, 317);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 21);
            this.label2.TabIndex = 1;
            this.label2.Text = "Last Name:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Teal;
            this.label3.Location = new System.Drawing.Point(335, 363);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 21);
            this.label3.TabIndex = 1;
            this.label3.Text = "Email:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Teal;
            this.label4.Location = new System.Drawing.Point(335, 409);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 21);
            this.label4.TabIndex = 1;
            this.label4.Text = "Adress:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Teal;
            this.label5.Location = new System.Drawing.Point(335, 225);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(86, 21);
            this.label5.TabIndex = 1;
            this.label5.Text = "Password:";
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblUserName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblUserName.Location = new System.Drawing.Point(432, 179);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(88, 21);
            this.lblUserName.TabIndex = 10;
            this.lblUserName.Text = "Username";
            // 
            // lblPassword
            // 
            this.lblPassword.AutoSize = true;
            this.lblPassword.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblPassword.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblPassword.Location = new System.Drawing.Point(420, 227);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(58, 21);
            this.lblPassword.TabIndex = 10;
            this.lblPassword.Text = "********";
            // 
            // lblFirstName
            // 
            this.lblFirstName.AutoSize = true;
            this.lblFirstName.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblFirstName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblFirstName.Location = new System.Drawing.Point(428, 271);
            this.lblFirstName.Name = "lblFirstName";
            this.lblFirstName.Size = new System.Drawing.Size(91, 21);
            this.lblFirstName.TabIndex = 10;
            this.lblFirstName.Text = "First Name";
            // 
            // lblLastName
            // 
            this.lblLastName.AutoSize = true;
            this.lblLastName.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblLastName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblLastName.Location = new System.Drawing.Point(431, 317);
            this.lblLastName.Name = "lblLastName";
            this.lblLastName.Size = new System.Drawing.Size(93, 21);
            this.lblLastName.TabIndex = 10;
            this.lblLastName.Text = "Last Name";
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblEmail.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblEmail.Location = new System.Drawing.Point(389, 363);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(51, 21);
            this.lblEmail.TabIndex = 10;
            this.lblEmail.Text = "Email";
            // 
            // msgBoxAdress
            // 
            this.msgBoxAdress.BackColor = System.Drawing.SystemColors.Control;
            this.msgBoxAdress.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.msgBoxAdress.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.msgBoxAdress.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.msgBoxAdress.Location = new System.Drawing.Point(398, 409);
            this.msgBoxAdress.Multiline = true;
            this.msgBoxAdress.Name = "msgBoxAdress";
            this.msgBoxAdress.ReadOnly = true;
            this.msgBoxAdress.Size = new System.Drawing.Size(176, 98);
            this.msgBoxAdress.TabIndex = 11;
            this.msgBoxAdress.Text = "Adress";
            // 
            // panelCart
            // 
            this.panelCart.BackColor = System.Drawing.SystemColors.Control;
            this.panelCart.Controls.Add(this.panel3);
            this.panelCart.Controls.Add(this.panel1);
            this.panelCart.Location = new System.Drawing.Point(577, 104);
            this.panelCart.Margin = new System.Windows.Forms.Padding(2);
            this.panelCart.Name = "panelCart";
            this.panelCart.Size = new System.Drawing.Size(351, 419);
            this.panelCart.TabIndex = 12;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.lstviewLogs);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 45);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(351, 374);
            this.panel3.TabIndex = 2;
            // 
            // lstviewLogs
            // 
            this.lstviewLogs.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.UserEvent,
            this.Date});
            this.lstviewLogs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstviewLogs.FullRowSelect = true;
            this.lstviewLogs.GridLines = true;
            this.lstviewLogs.HideSelection = false;
            this.lstviewLogs.Location = new System.Drawing.Point(0, 0);
            this.lstviewLogs.MultiSelect = false;
            this.lstviewLogs.Name = "lstviewLogs";
            this.lstviewLogs.Size = new System.Drawing.Size(351, 374);
            this.lstviewLogs.TabIndex = 0;
            this.lstviewLogs.UseCompatibleStateImageBehavior = false;
            this.lstviewLogs.View = System.Windows.Forms.View.Details;
            // 
            // UserEvent
            // 
            this.UserEvent.Text = "User Event";
            this.UserEvent.Width = 195;
            // 
            // Date
            // 
            this.Date.Text = "Date";
            this.Date.Width = 135;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Teal;
            this.panel1.Controls.Add(this.lblUserNameLog);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(351, 45);
            this.panel1.TabIndex = 0;
            // 
            // lblUserNameLog
            // 
            this.lblUserNameLog.AutoSize = true;
            this.lblUserNameLog.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblUserNameLog.ForeColor = System.Drawing.SystemColors.Control;
            this.lblUserNameLog.Location = new System.Drawing.Point(14, 13);
            this.lblUserNameLog.Name = "lblUserNameLog";
            this.lblUserNameLog.Size = new System.Drawing.Size(80, 21);
            this.lblUserNameLog.TabIndex = 0;
            this.lblUserNameLog.Text = "User Logs";
            // 
            // btnPasswordChange
            // 
            this.btnPasswordChange.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnPasswordChange.BackColor = System.Drawing.Color.Teal;
            this.btnPasswordChange.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPasswordChange.BorderRadius = 0;
            this.btnPasswordChange.ButtonText = "Change Password";
            this.btnPasswordChange.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPasswordChange.DisabledColor = System.Drawing.Color.Gray;
            this.btnPasswordChange.Iconcolor = System.Drawing.Color.Transparent;
            this.btnPasswordChange.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnPasswordChange.Iconimage")));
            this.btnPasswordChange.Iconimage_right = null;
            this.btnPasswordChange.Iconimage_right_Selected = null;
            this.btnPasswordChange.Iconimage_Selected = null;
            this.btnPasswordChange.IconMarginLeft = 0;
            this.btnPasswordChange.IconMarginRight = 0;
            this.btnPasswordChange.IconRightVisible = true;
            this.btnPasswordChange.IconRightZoom = 0D;
            this.btnPasswordChange.IconVisible = true;
            this.btnPasswordChange.IconZoom = 50D;
            this.btnPasswordChange.IsTab = false;
            this.btnPasswordChange.Location = new System.Drawing.Point(11, 27);
            this.btnPasswordChange.Margin = new System.Windows.Forms.Padding(4);
            this.btnPasswordChange.Name = "btnPasswordChange";
            this.btnPasswordChange.Normalcolor = System.Drawing.Color.Teal;
            this.btnPasswordChange.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnPasswordChange.OnHoverTextColor = System.Drawing.Color.White;
            this.btnPasswordChange.selected = false;
            this.btnPasswordChange.Size = new System.Drawing.Size(180, 50);
            this.btnPasswordChange.TabIndex = 15;
            this.btnPasswordChange.Text = "Change Password";
            this.btnPasswordChange.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPasswordChange.Textcolor = System.Drawing.Color.White;
            this.btnPasswordChange.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPasswordChange.Click += new System.EventHandler(this.btnPasswordChange_Click);
            // 
            // btnAdressChange
            // 
            this.btnAdressChange.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnAdressChange.BackColor = System.Drawing.Color.Teal;
            this.btnAdressChange.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAdressChange.BorderRadius = 0;
            this.btnAdressChange.ButtonText = "Change Adress";
            this.btnAdressChange.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAdressChange.DisabledColor = System.Drawing.Color.Gray;
            this.btnAdressChange.Iconcolor = System.Drawing.Color.Transparent;
            this.btnAdressChange.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnAdressChange.Iconimage")));
            this.btnAdressChange.Iconimage_right = null;
            this.btnAdressChange.Iconimage_right_Selected = null;
            this.btnAdressChange.Iconimage_Selected = null;
            this.btnAdressChange.IconMarginLeft = 0;
            this.btnAdressChange.IconMarginRight = 0;
            this.btnAdressChange.IconRightVisible = true;
            this.btnAdressChange.IconRightZoom = 0D;
            this.btnAdressChange.IconVisible = true;
            this.btnAdressChange.IconZoom = 50D;
            this.btnAdressChange.IsTab = false;
            this.btnAdressChange.Location = new System.Drawing.Point(398, 27);
            this.btnAdressChange.Margin = new System.Windows.Forms.Padding(4);
            this.btnAdressChange.Name = "btnAdressChange";
            this.btnAdressChange.Normalcolor = System.Drawing.Color.Teal;
            this.btnAdressChange.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnAdressChange.OnHoverTextColor = System.Drawing.Color.White;
            this.btnAdressChange.selected = false;
            this.btnAdressChange.Size = new System.Drawing.Size(180, 50);
            this.btnAdressChange.TabIndex = 14;
            this.btnAdressChange.Text = "Change Adress";
            this.btnAdressChange.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAdressChange.Textcolor = System.Drawing.Color.White;
            this.btnAdressChange.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdressChange.Click += new System.EventHandler(this.btnAdressChange_Click);
            // 
            // btnEmailChange
            // 
            this.btnEmailChange.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnEmailChange.BackColor = System.Drawing.Color.Teal;
            this.btnEmailChange.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnEmailChange.BorderRadius = 0;
            this.btnEmailChange.ButtonText = "Change Email";
            this.btnEmailChange.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEmailChange.DisabledColor = System.Drawing.Color.Gray;
            this.btnEmailChange.Iconcolor = System.Drawing.Color.Transparent;
            this.btnEmailChange.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnEmailChange.Iconimage")));
            this.btnEmailChange.Iconimage_right = null;
            this.btnEmailChange.Iconimage_right_Selected = null;
            this.btnEmailChange.Iconimage_Selected = null;
            this.btnEmailChange.IconMarginLeft = 0;
            this.btnEmailChange.IconMarginRight = 0;
            this.btnEmailChange.IconRightVisible = true;
            this.btnEmailChange.IconRightZoom = 0D;
            this.btnEmailChange.IconVisible = true;
            this.btnEmailChange.IconZoom = 60D;
            this.btnEmailChange.IsTab = false;
            this.btnEmailChange.Location = new System.Drawing.Point(204, 27);
            this.btnEmailChange.Margin = new System.Windows.Forms.Padding(4);
            this.btnEmailChange.Name = "btnEmailChange";
            this.btnEmailChange.Normalcolor = System.Drawing.Color.Teal;
            this.btnEmailChange.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnEmailChange.OnHoverTextColor = System.Drawing.Color.White;
            this.btnEmailChange.selected = false;
            this.btnEmailChange.Size = new System.Drawing.Size(180, 50);
            this.btnEmailChange.TabIndex = 13;
            this.btnEmailChange.Text = "Change Email";
            this.btnEmailChange.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEmailChange.Textcolor = System.Drawing.Color.White;
            this.btnEmailChange.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEmailChange.Click += new System.EventHandler(this.btnEmailChange_Click);
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(933, 564);
            this.ControlBox = false;
            this.Controls.Add(this.btnPasswordChange);
            this.Controls.Add(this.btnAdressChange);
            this.Controls.Add(this.btnEmailChange);
            this.Controls.Add(this.panelCart);
            this.Controls.Add(this.msgBoxAdress);
            this.Controls.Add(this.lblEmail);
            this.Controls.Add(this.lblLastName);
            this.Controls.Add(this.lblFirstName);
            this.Controls.Add(this.lblPassword);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "SettingsForm";
            this.Text = "HomePageForm";
            this.Load += new System.EventHandler(this.SettingsForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panelCart.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.Label lblFirstName;
        private System.Windows.Forms.Label lblLastName;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.TextBox msgBoxAdress;
        private System.Windows.Forms.Panel panelCart;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ListView lstviewLogs;
        private System.Windows.Forms.ColumnHeader UserEvent;
        private System.Windows.Forms.ColumnHeader Date;
        private System.Windows.Forms.Panel panel1;
        private Bunifu.Framework.UI.BunifuCustomLabel lblUserNameLog;
        private Bunifu.Framework.UI.BunifuFlatButton btnPasswordChange;
        private Bunifu.Framework.UI.BunifuFlatButton btnAdressChange;
        private Bunifu.Framework.UI.BunifuFlatButton btnEmailChange;
    }
}